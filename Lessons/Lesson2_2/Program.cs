﻿/* Теми уроку
- Методи класу String
- Інтерполяція в стрічках
- Буквальні стрічки
*/

using System;

namespace Lesson2_2
{
	class Program
	{
		static void Main(string[] args)
		{
			string myMessage = "    Hello my name name is Sasha    ";

			//Length довжина стрічки
			int mystringLength = myMessage.Length; //30
			Console.WriteLine(mystringLength);

			//Забрати пробіли з кінця/початку або всі
			string trimedString1 = myMessage.TrimEnd(); // "Hello my name is Sasha"
			Console.WriteLine(trimedString1);

			string trimedString2 = myMessage.TrimStart(); // "Hello my name is Sasha    "
			Console.WriteLine(trimedString2);

			string trimedString3 = myMessage.Trim(); // "Hello my name is Sasha"
			Console.WriteLine(trimedString3);

			char[] test = myMessage.ToCharArray();
			char element = test[5]; //e

			Console.WriteLine(myMessage.ToUpper());
			Console.WriteLine(myMessage.ToLower());
			Console.WriteLine(myMessage.Replace("Sasha","Vova"));
			Console.WriteLine(myMessage.Remove(5, 10));
			Console.WriteLine(myMessage.LastIndexOf('s'));
			Console.WriteLine(myMessage.LastIndexOf("name"));
			Console.WriteLine("sasha".Equals("vova"));
			Console.WriteLine("sasha".Equals("sasha"));
			Console.WriteLine(element);

			int myIntValue = 10;
			string myString = string.Format("Part of string {0} another part of string", myIntValue);
			string myString1 = $"Part of string {myIntValue} another part of string";
			Console.WriteLine(myString);

			//Екранування спец-символів
			string specialCharacters = "Hello \n next line \x7E";
			Console.WriteLine(specialCharacters);
			string url = "C:\\file\\no"; //  "C:\file\no"

			//Оголошення буквальноъ стрычки
			string bstring = @"C:fileno
sdfgsdfg \x7E
sdfg
sdfg
sdfg
sdfgsdfg";
			Console.WriteLine(bstring);
			Console.ReadKey();
		}
	}
}