﻿/*
 Теми уроку
 Типи операторів
 Оператори роботи з числовими типами
 Оператори логічних типів
 
 */
using System;

namespace Lesson2
{
	class Program
	{
		/// <summary>
		/// Функція1 для пояснення того яка різниця між & та &&
		/// </summary>
		/// <returns></returns>
		static bool TestTrueFunc()
		{
			Console.WriteLine("TestTrueFunc");
			return true;
		}

		/// <summary>
		/// Функція2 для пояснення того яка різниця між & та &&
		/// </summary>
		/// <returns></returns>
		static bool TestFalseFunc()
		{
			Console.WriteLine("TestFalseFunc");
			return false;
		}

		//Операції можуть бути унарними бінарними і тернарними
		//Унарні  (x оператор) або (оператор x)
		//Бінарні (x оператор y)
		//Тернарні (x оператор1 y оператор2 z)
		static void Main(string[] args)
		{
			#region Унарні

			// ++x --x x++ x-- -x
			int s = 10;
			int s2 = 10;

			int signedS = -s;
			int signedS2 = s * -1;

			Console.WriteLine(s++); //10
			Console.WriteLine(s); //11

			Console.WriteLine(++s2); //11
			Console.WriteLine(s2); //11

			//s = 11
			// - - - - 11 +  11 - 10
			/*int test = s++ + --s - --s;
			Console.WriteLine(s);*/
			
			//суфіксний інкремент
			s++;
			//суфіксний декремент
			s--;
			//префіксний декремент
			--s;
			//префіксний інкремент
			++s;

			#endregion

			#region Бінарні оператори

			int x = 10;

			//оператор % повертає остачу від ділення
			int r0 = 13 % 5; //3      13/5  націло не ділиться   10/5=2      13-10 = 3

			int r1 =  x + 10; //20
			Console.WriteLine("x + 10 = {0}", r1); //x + 10 = 20

			int r2 = x * r1;
			Console.WriteLine("x * r1 = {0}", r2); //x * r1 = 200

			int r3 = x / 3; //3.33333333 => 3
			Console.WriteLine("x * r2 = {0}", r3); //x * r2 = 3

			//x це int 3 це int тому компілятор повертає результат int
			//якщо написати (double) то компілятор знатиме про те, що результат має бути дробовим
			double r4 = (double)x / 3; //3
			Console.WriteLine("(double)x / 3 = {0}", r4); //(double)x / 3 = 3.33333

			//2.5 це double r4 це int тому компілятор повертає результат int
			//У випадку коли один з оперантів double - результат теж буде double
			double r5 = 2.5 / r4;
			Console.WriteLine("2.5 / r4 = {0}", r5);

			/*
			int r6 = x * r1;
			Console.WriteLine("x * r1 = {0}", r2); //200
			*/

			int t = (25 + 10) * 44 - (4 - 5);

			#endregion

			#region Побітові оператори

			// x>> x<< оператори зсуву
			//       -0100- >> -0010-
			int bit1 = 4    >>    1; //2

			//-0100- >> -1000-
			int bit2 = 4 << 1; //8

			#endregion

			#region Логічні операції

			// працюють тільки з true або false

			//& і AND повертає true тільки в тому випадку якщо обидва операнти true в іншому випадку false
			bool b1 = true & true; //true
			bool b2 = true & false; //false
			bool b3 = false & false; //false

			//| або OR повертає true в тому випадку якщо хоча б один з оперантів true
			bool b4 = true | true; //true
			bool b5 = true | false; //true
			bool b6 = false | false; //false

			//^ або XOR - виключне або повертає true в тому випадку якщо значення оперантів різні
			bool b7 = false ^ false; //false
			bool b8 = true ^ false; //true
			bool b9 = false ^ true; //true
			bool b10 = true ^ true; //false

			//              false      &       true       //false
			bool b11 = TestFalseFunc() & TestTrueFunc();

			//              false      &&       true       //false
			//У випадку якщо перший оперант false - другий не буде перевірятися компілятором
			bool b12 = TestFalseFunc() && TestTrueFunc();
			//У випадку якщо перший оперант true - другий не буде перевірятися компілятором
			bool b13 = TestFalseFunc() || TestTrueFunc();

			//! NOT не - перетворює в зворотнє
			bool b14 = !true; // false
			bool b15 = !false; // true

			//Логічний вираз

			bool b16 = (true && false) || (false && (!false || true) && TestTrueFunc()); //false

			#endregion

			#region Операції порівняння

			int test = 10;
			bool b17 = 20 > test; //true
			bool b18 = 10 > test; //false
			bool b19 = 10 >= test; //true

			bool b20 = 10 < test; //false
			bool b21 = 9 < test; //true

			#endregion

			#region Тернарний оператор

			//x ? y : z - якщо x це true, то повертається y. В іншому випадку повертається z
			int ternar1 = true ? 10 : 20; //10
			int ternar2 = false ? 10 : 20; //20
			int tern3 = false ? true ? 10 : 50 : 30;

			#endregion

			Console.ReadKey();
		}
	}
}