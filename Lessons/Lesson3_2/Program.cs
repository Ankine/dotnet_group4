﻿/* 
 Теми уроку:
 Області видимості 
 Простори імен

 Правила
 Правило 1 - ніяка частина коду не може існувати поза простором імен
 Правило 2 - файл може містити два і більше просторів імен
 Правило 3 - Один простір імен може бути в декількох файлах і при його підключенні дає доступ до сутностей оголошених в цих файлах
 Праввило 4 - Можна не підключати простір імен через using
*/

using System;
//using Lesson3_2.Lib;

namespace Lesson3_2.MyNameSpace1
{
}

namespace Lesson3_2.MyNameSpace2
{
	public static class Part2NameSpace2
	{
		public static string Value = "Hello World from part2";
	}
}

namespace Lesson3_2
{
	//Правило 1 змінна зникає з областей видимості після закриття тіла де вона була оголошена
	//Правило 2 змінна доступна у всіх вкладеннях поточного тіла
	//тіло 1-го рівня вкладеності
	class Program
	{
		//тіло 2-го рівня вкладеності
		static void Main(string[] args)
		{
			bool boolValue1; //false - default value for boolean types
			bool.TryParse(Console.ReadLine(), out boolValue1);
			//тіло 3-го рівня вкладеності
			if (boolValue1) //boolValue = true
			{
				int intValue = 15;

				//тіло 4-го рівня вкладеності
				boolValue1 = true; //змінна доступна
				if (true)
				{
					intValue = 10;
					//тіло 5-го рівня вкладеності
					boolValue1 = false; //змінна доступна
				}
			}
			else //boolValue == false or any another strings in console
			{
				//тіло 4-го рівня вкладеності
				
				boolValue1 = true; //змінна доступна
			}

			//Console.WriteLine(MyClass.Value); - можна писати, якщо є using Lesson3_2.Lib;
			Console.WriteLine(Lesson3_2.Lib.MyClass.Value); //15
			//intValue - тут стає недоступною, бо вона зникає після закриття тіла де вона оголошена
		}
	}
}