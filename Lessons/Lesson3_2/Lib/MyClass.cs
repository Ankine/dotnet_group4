﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson3_2.MyNameSpace2;

namespace Lesson3_2.Lib
{
	public static class MyClass
	{
		public static int Value = 15;

		public static string GetMessage()
		{
			return Part2NameSpace2.Value + Part1NameSpace2.Value;
		}
	}
}
