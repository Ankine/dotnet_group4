﻿/*
 * Перегрузка або Перевантаження операторів (operators overloading)
 * Перегружати можна true false *+-/ == != (> <) (<= >=) ++ --
 * Неможна перегружати =, ., ?:, ??, ->, =>, f(x), as, checked, unchecked, default, delegate, is, new, sizeof, typeof 
 * https://docs.microsoft.com/ru-ru/dotnet/csharp/programming-guide/statements-expressions-operators/overloadable-operators
*/

using System;

namespace Lesson12_2
{
	public class Line
	{
		public Line(Point start, Point end)
		{
			Start = start;
			End = end;
		}

		public Point Start { get; set; }
		public Point End { get; set; }

		//Буде потребувати каста
		public static explicit operator Point(Line line)
		{
			return new Point(line.Start.X, line.End.Y);
		}

		//Можна приводити без каста
		public static implicit operator Line(Point point)
		{
			return new Line(point, point);
		}

	}

	public class Point
	{
		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}

		public int X { get; set; }
		public int Y { get; set; }

		public static Point operator +(Point p1, Point p2)
		{
			return new Point(p1.X + p2.X, p1.Y + p2.Y);
		}

		public static bool operator >(Point p1, Point p2)
		{
			return p1.X + p1.Y > p2.X + p2.Y;
		}

		public static bool operator <=(Point p1, Point p2)
		{
			return p1.X + p1.Y <= p2.X + p2.Y;
		}

		public static bool operator >=(Point p1, Point p2)
		{
			return p1.X + p1.Y >= p2.X + p2.Y;
		}

		public static bool operator <(Point p1, Point p2)
		{
			return p1.X + p1.Y < p2.X + p2.Y;
		}

		public static Point operator -(Point p1)
		{
			return new Point(-p1.X,-p1.Y);
		}

		public double GetLineLengthToZeroPoint()
		{
			return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
		}

		public override string ToString()
		{
			return $"X={X} Y={Y}";
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			//перегрузка операторів
			var p1 = new Point(10,20);
			var p2 = new Point(30,40);
			var p3 = p1 + p2;
			Console.WriteLine($"P1 {p1} + P2 {p2} = P3 {p3}");
			Console.WriteLine(p1 < p2);
			Console.WriteLine(-p2);

			//Приведення мыж класами
			var line = new Line(p1, p2);
			var p4 = (Point)line; //треба точку X = line.Start.X Y = line.End.Y
			Line line2 = (Line)p4;

			Console.ReadKey();
		}
	}
}
