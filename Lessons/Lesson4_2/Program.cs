﻿/*
 Тема Уроку
 Перечислення

 Правила
 Поравило 1 - неявним типом перечислення являється тип int
 Правило 2 - старатися виділяти змінну найменшого розміру
 */
using System;


namespace Lesson4_2
{
	enum DayOfWeek : byte //byte, short, int, long
	{
		Sunday,
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday
	}

	class Program
	{
		static void Main(string[] args)
		{
			DayOfWeek day1 = DayOfWeek.Monday;
			DayOfWeek day2 = DayOfWeek.Wednesday;

			if (day2 > DayOfWeek.Sunday) //true
			{
			}

			Console.WriteLine((int)day1);
			Console.WriteLine(day1);

			Console.WriteLine(day1 == DayOfWeek.Monday && day2 == DayOfWeek.Tuesday); //true
			Console.WriteLine(day1 == day2); //false

			//try parse
			DayOfWeek day3;
			if (Enum.TryParse(Console.ReadLine(), out day3))
			{
				Console.WriteLine((int)day3);
			}

			int intValue = 2;
			DayOfWeek day4 = (DayOfWeek)intValue;

			Console.ReadKey();
		}
	}
}
