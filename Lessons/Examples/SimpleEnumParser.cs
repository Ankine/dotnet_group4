﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
	public static class SimpleEnumParser
	{
		private static int GetValidEnumValue(Type enumType)
		{
			while (true)
			{
				Console.WriteLine($"Please choose {enumType.Name}");
				foreach (var value in Enum.GetValues(enumType))
					Console.WriteLine($"{(int)value} {value}");
				if (int.TryParse(Console.ReadLine(), out int enumId) && Enum.IsDefined(enumType, enumId))
					return enumId;
				Console.WriteLine("Value is not valid");
			}
		}
	}
}
