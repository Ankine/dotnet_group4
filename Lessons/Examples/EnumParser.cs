﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Examples
{
	/// <summary>
	/// Class that can be used to parse enumerations
	/// </summary>
	public sealed class EnumParser : IEnumParser
	{
		#region Data Members

		private readonly Action<string> _outPutMethod;
		private readonly Func<string> _inputMethod;

		#endregion

		#region Constants

		private const string PARSE_FAILED_MESSAGE = "Value is not valid";
		private const string PLEASE_CHOOSE = "Please choose";

		#endregion

		#region Constructors

		/// <summary>
		/// Creates EnumParser
		/// </summary>
		/// <param name="outPutMethod">Action to show data</param>
		/// <param name="inputMethod">Fanction to input data</param>
		public EnumParser(Action<string> outPutMethod, Func<string> inputMethod)
		{
			_outPutMethod = outPutMethod;
			_inputMethod = inputMethod;
		}

		#endregion

		#region Public Methods

		public TEnum ConsoleReadEnum<TEnum>() where TEnum : struct, IConvertible
		{
			return GetValidEnumValue<TEnum>(_inputMethod, (name, values) =>
			{
				_outPutMethod($"Please choose {name}");
				values.ForEach(_outPutMethod);
			}, () => _outPutMethod(PARSE_FAILED_MESSAGE));
		}

		#endregion

		#region Private Methods

		private TEnum GetValidEnumValue<TEnum>(Func<string> getValueToParse, Action<string, List<string>> preParseAction, Action parseFailedAction)
			where TEnum : struct, IConvertible
		{
			var type = typeof(TEnum);
			var list = (from object value in Enum.GetValues(type) select $"{(int)value} {value}").ToList();
			while (true)
			{
				preParseAction(type.Name, list);
				if (int.TryParse(getValueToParse(), out int enumId) && Enum.IsDefined(type, enumId))
					return (TEnum)(object)enumId;
				parseFailedAction();
			}
		}

		#endregion
	}
}
