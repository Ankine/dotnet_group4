﻿using System;

namespace Examples
{
	public class Class
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку");
            string input;
            input = Console.ReadLine();

            if (input == "")
            {
                Console.WriteLine("Вы ввели простую строку");
            }
            else if(input.Length < 5)
            {
                Console.WriteLine("Вы ввели строку меньше 10 символов");
            }
            else if(input.Length < 10)
            {
                Console.WriteLine("Вы ввели строку больше 5 символов");
            }

            Console.WriteLine("Была введена строка" + input);
            Console.ReadLine();
        }
    }
}
