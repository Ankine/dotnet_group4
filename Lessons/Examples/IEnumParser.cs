﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
	/// <summary>
	/// Contract for classes to parse enumerations
	/// </summary>
	public interface IEnumParser
	{
		/// <summary>
		/// Convert data typed in console to specified type of enumeration
		/// </summary>
		/// <typeparam name="TEnum">Type of enumeration</typeparam>
		/// <returns>Enumeration value</returns>
		TEnum ConsoleReadEnum<TEnum>() where TEnum : struct, IConvertible;
	}
}
