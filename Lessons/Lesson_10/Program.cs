﻿/*
	Частинне оголошення класів (Partial classes)
	Методи розширення (Extentions methods)

Частинне оголошення класів
 Правило 1 - Частинні класи дозволяють поміщати контент одного і того ж класу в декілька різних файлів
 Правило 2 - Простори імен для частинних класів повинні бути однакові, бо інакше це два різних класи
 Правило 3 - Використовується ключове слово partial

 Методи розширення
 Правило 1 - Для створення методів розширення трема мати статичний клас
 Правило 2 - В методі розширення тип для першого параметра повинен бути відмічений ключовим словом this
			тоді це метод стає методом розширення для вказаного типу
 Правило 3 - Окрім пр1 пр2 це звичайний метод, що може мати довільну кількість додаткових параметрів та довільний тип повернення
 */

namespace Lesson_10
{
	public static class StringExtentions
	{
		public static int GetCountOf(this string mystring, char character)
		{
			var array = mystring.ToCharArray();
			var count = 0;
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] == character)
				{
					count ++;
				}
			}
			return count;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			var person = new PersonCard();

			var refValue = 1;
			person.Method3(ref refValue, "sdfsdf");
			//refValue = 10
			byte myByteValue;
			person.Method4("", out myByteValue);
			person.Method5("test",val3:2);

			var myStringObject = "sasha";

			//Методи розширення
			var countOfC = myStringObject.GetCountOf('a'); //2
		}
	}
}
