﻿/*
 * Методи класів
 * Метод - це функція всередині класу, яка використовується для роботи з даними класу
 */

namespace Lesson_10
{
	public partial class PersonCard
	{
		public int Id { get; set; }
		public long Balance { get; set; }

		/// <summary>
		/// Звичайний метод без параметрів та повернення
		/// </summary>
		public void Method1()
		{
		}

		/// <summary>
		/// Якщо метод має повернення то воно повинно спрацьовувати у всіх розгалуженнях в тілі методу
		/// </summary>
		/// <param name="param">просто тесто змінна для реалізації розгалуження</param>
		/// <returns></returns>
		public int Method2(bool param)
		{
			if (param)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}

		/// <summary>
		/// Метод з декількома параметрами і ref параметром
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		public void Method3(ref int v1, string v2)
		{
			v1 = 10;
		}

		/// <summary>
		/// Метод з out параметром
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		public void Method4(string v1, out byte v2) // void (string, out byte)
		{
			v2 = 10;
		}

		/// <summary>
		/// Приклад overload 1
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		public void Method4(string v1, byte v2) // void (string, byte)
		{
			v2 = 10;
		}

		/// <summary>
		/// Приклад overload 2
		/// </summary>
		/// <param name="v1"></param>
		public void Method4(string v1) // void (string)
		{
		}

		/// <summary>
		/// Метод з необов'язковими параметрами
		/// </summary>
		public void Method5(string val1, int val2 = 10, long val3 = 10)
		{
		}
	}
}