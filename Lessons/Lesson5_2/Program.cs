﻿//------------------------------
//Функції з необов'язковими параметрами
//Перегрузку функцій
//------------------------------

//Правило 1 - Параметри, що мають присвоєне значення називаються необов'язковими
//Правило 2 - необов'язкові параметри повинні іти вкінці (за обов'язковими)
//Правило 3 - необов'язкові параметри можна передавати частково, якщо не в парвильному порядку, то треба передавати по назві через :

//Перегрузка функцій
//Правило 1 - Перегружені фінкції мають відрізнятися: 
				//За типом параметрів
				//Кількістю параметрів
				//Порядком слідування параметрів
			// Не можуть відрізнятися за назвою параметрів або за типом повернення
//Правило 2 - Якщо метод має іншу назву, то це вже не перегрузка а просто інший метод

using System;

namespace Lesson5_2
{
	class Program
	{
		public static void Function1(int x, string y = "hello", bool z = false)
		{
			Console.WriteLine($"x = {x} y = {y} z = {z}");
		}
		public static void Function2()
		{
		}

		public static void Function2(int x)
		{
		}

		public static void Function2(ref int x)
		{
		}

		public static void Function2(int x, string y)
		{
		}

		public static void Function2(string x, int y)
		{
		}

		public static void Function2(string x)
		{
		}

		public static void OutFunction3(out int outIntParam)
		{
			outIntParam = 32;
		}

		public static void RefFunction(ref int refParameter)
		{
			refParameter = 10;
		}

		public static void NotRefFunction(int notRefParameter)
		{
			notRefParameter = 10;
		}

		static void Main(string[] args)
		{
			//варіант1 - передаються всі параметри, тоді всі необов'язкові параметри 
			//замінюються на передані значення
			Function1(12, "test 1"); //x = 12 y = test 1 z = false

			//варіант2 - передаються тільки обов'язкові параметри, тоді всі необов'язкові 
			//будуть використовувати стандартні значення
			Function1(13); //x = 13 y = hello z = false

			//варіант 3.1 - необов'язкові параметри передаються частково в правильному порядку
			Function1(14, "hi"); //x = 14 y = hi z = false
			//варіант 3.2 - необов'язкові параметри передаються частково в неправильному порядку
			//параметри передаються по назві через крапку з комою
			Function1(15, z:true, y:"sdfgsdfg"); //x = 15 y = hello z = true

			int notInitializedVariable;
			OutFunction3(out notInitializedVariable);

			int refVariable = 150;
			NotRefFunction(refVariable);
			//refVariable = 150
			RefFunction(ref refVariable);
			//refVariable = 10

			Program.Function2();
			Program.Function2("sdfs",10);
			Program.Function2("sdfs");


			Console.ReadKey();
		}
	}
}
