﻿/*
 Теми уроку
 Оператори розгалуження
 Оператор if
 Оператор switch-case
 */

using System;

namespace Lesson3_3
{
	//До розгалужень відносяться частини коду які можуть виконуватися або не виконуватися
	//залежності від певної умови
	//if-else if-else / switch / goto - забути / conditional operator / null cohesion operator

	//operator if
	//Правило 1 ключове слово if пишеться завжди
	//Паавило 2 else if або else - опціональні
	//Привило 3 якщо використано else, то else if писати не можна
	//правило 4 блоків else if може бути безліч


	//operator switch-case
	//Правило 1 switch-case оператор буде виконувати частину коду в case у випадку якщо значення switch збігається з значенням case
	//Правило 2 switch-case буде виконувати наступне значення, якщо не знайде break
	//Правило 3 switch-case буде виконувати блок default, якщо значення не знайшлося у світчах
	class Program
	{
		static void Main(string[] args)
		{
			#region if operator

			bool condition = true; //or false
			//case 1----------------------------------------
			if (condition)
			{
				//code can be performed if condition is true
			}

			//case 2----------------------------------------
			if (condition)
			{
				//code can be performed if condition is true
			}
			else
			{
				//code can be performed if condition is false
			}

			//case 3----------------------------------------
			var condition2 = !condition;
			if (condition)
			{
				//code can be performed if condition is true
			}
			else if (condition2)
			{
				//code can be performed if condition is false but condition2 is true
			}
			else
			{
				//code can be performed if condition is false and condition2 is false
			}

			#endregion

			#region switch-case operator

			var intValue = Convert.ToInt32(Console.ReadLine());
			//Оператором множинного вибору

			//case 1
			//1 - Hello Sasha/ 2 - Hello Vova /3 - Hello Igor
			switch (intValue)
			{
				case 1: Console.WriteLine("Hello Sasha"); break;
				case 2: Console.WriteLine("Hello Vova"); break;
				case 3: Console.WriteLine("Hello Igor"); break;
			}

			//case 1
			//1 або 2 - Hello Vova /3 - Hello Igor
			switch (intValue)
			{
				case 1: 
				case 2: Console.WriteLine("Hello Vova"); break;
				case 3: Console.WriteLine("Hello Igor"); break;
			}

			//case 1
			//1 або 2 - Hello Vova /3 - Hello Igor
			switch (4)
			{
				case 1:
				case 2:
				{
					Console.WriteLine("Hello Sasha");
					Console.WriteLine("Hello Vova"); break;
				}
				case 3: Console.WriteLine("Hello Igor"); break;
				default: Console.WriteLine("Default"); break;
			}

			Console.ReadKey();

			#endregion
		}
	}
}