﻿/*
 * Узагальнені делегати (узагальнені класи, що описують делегати певної сигнатури) в .NET (Action/Func)
 * Action<T>,Action<T,T>,Action<T,T,T> до 16
 * Func<T> ... до 16
 * Правило 1 Action - це системний (.NET) делегат без типу повернення але з параметрами (або без параметрів)
 * Правило 2 Func - це системний (.NET) делегат з типом повернення та параматреми (або без параметрів)
 * Правило 3 Для Func останнім узагальненим параметром являється тип повернення делегату а всі решта вхідними параметрами
*/

using System;

namespace Lesson_11_3
{
	//Action
	public delegate void Test1(int val);
	public delegate void Test2(int val,string val2,double val3);

	//Func
	public delegate int Test3();
	public delegate string Test4(int val1);


	class Program
	{
		#region Delegates

		//Action
		public static Action D0_Generic { get; set; }

		public static Test1 D1 { get; set; }
		public static Action<int> D1_Generic { get; set; }

		public static Test2 D2 { get; set; }
		public static Action<int, string, double> D2_Generic { get; set; }

		//Func
		public static Func<int> D3_Generic { get; set; }
		public static Func<int,string> D4_Generic { get; set; }


		//Hard examples
		public static Func<Action<string,int>> BrainFuck { get; set; }
		public static Func<Action<int>,string> BrainFuck2 { get; set; }

		public static Action<Func<string,int>, double> BrainFuck3 { get; set; }

		#endregion

		#region Methods

		public static void MethodForBrainFuck3(Func<string, int> val1, double val2)
		{
		}

		public static string MethodForBrainFuck2(Action<int> val)
		{
			return string.Empty;
		}

		public static Action<string, int> MyMethodForBrainFuck()
		{
			return ReturnTypeForBrainFuck;
		}

		public static void ReturnTypeForBrainFuck(string val1, int val2)
		{
		}

		public static int MyMethodForD3()
		{
			return 10;
		}

		public static string MyMethodForD4(int val)
		{
			return string.Empty;
		}

		#endregion

		static void Main(string[] args)
		{
			D0_Generic();
			D1_Generic(10);
			D2_Generic(10, "sdfsdf", 2.5);

			D3_Generic += MyMethodForD3;
			D4_Generic += MyMethodForD4;
			BrainFuck += MyMethodForBrainFuck;
			BrainFuck3 += MethodForBrainFuck3;

			var test = BrainFuck();
			test("sdfgsd", 10);


			int d3_result = D3_Generic();
			string d4_result = D4_Generic(10);
		}
	}
}
