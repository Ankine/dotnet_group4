﻿/* Теми уроку
 Цикли
 Цикл for
 Цикл while
 Цикл do-while

 Правила:
 Правило 1 Якщо умова для початкового числа не true, то цикл не виконується
 Правило 2 Одне проходження циклю називається ітерацією
 Правило 3 Цикл повинен завжди мати умову виходу, бо в іншому випадку програма зависне (безкінечний цикл)
 Правило 4 Можна використовувати цикл для декількох змінних і цикл в циклі в циклі в циклі etc...
*/

using System;

//Дві категорії
//1 - цикли з передумовою for/foreach/while
//2 - цикли з післяумовою do-while
namespace Lesson3_4
{
	class Program
	{
		static void Main(string[] args)
		{
			#region For

			//for([ініціалізація ітератора];[умова виходу з циклу];[зміна ітератора])
			//{
			//}
			//for(initial; condition; loop)
			//case 1
			for (int myValue = 0; myValue < 10; myValue++)
			{
				if (myValue == 5)
				{
					continue; //пропустити поточну ітерацію циклу
				}
				if (myValue == 8)
				{
					break; //примусовий вихід з циклу
				}
				Console.WriteLine(myValue);
				//цикл збільшення
				//виконається 10 разів
			}
			//case 2
			for (int myValue = 20; myValue == 0; --myValue)
			{
				//цикл зменшення
			}

			//case 3
			for (int myValue = 0, myValue2 = 30; myValue != myValue2; myValue++, myValue2--)
			{
				Console.WriteLine(myValue + " " + myValue2);
			}

			//case 4
			for (int myValue = 20; myValue == 0; --myValue)
			{
				for (int secondValue = 20; secondValue == 0; --secondValue)
				{
					Console.WriteLine(myValue + " " + secondValue);
				}
			}

			#endregion

			#region While

			var intValue = 10;
			while (intValue>2)
			{
				//code
				intValue = intValue - 2;
			}
			//intValue = 2

			#endregion

			#region Do while

			intValue = 5;
			do
			{
				intValue++;
				//code
			} while (intValue < 1);
			//intValue = 6

			#endregion

			#region Foreach

			//Правило 1 не можна виконувати присвоєння в циклі foreach
			string[] stringArray = {"Hello", "My", "Name", "Is", "Sasha"};

			foreach (var el in stringArray) //це все що реалізує IEnumerable
			{
				Console.WriteLine(el);
			}

			#endregion

			Console.ReadKey();
		}
	}
}