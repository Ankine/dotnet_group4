﻿/* 
 Підключення інших просторів імен. Робота з класом Console
 Теми уроку
 Структура консольної програми
 Коментарі та документація
 Використання класу Console
*/
using System;

//Простір імен
namespace Lesson1
{
	class Program
	{
		//Документація
		/// <summary>
		/// Точка входу в програму
		/// </summary>
		/// <param name="args">Агрументи програми з моїм іменем</param>
		static void Main(string[] args)
		{
			//Одностроковий коментар

			/* Багато
				строковий
				коментар*/
			//Console.WindowWidth = 20;

			//Вивід тексту
			Console.Write("test text1");
			//Вивід тексту і перехід на новий рядок
			Console.WriteLine("test text2");
			//Вивід форматованого тесту
			int integerValue1 = 15;
			bool booleanValue2 = false;
			//Форматований вивід
			Console.WriteLine("{1} test text {0}", integerValue1, booleanValue2);

			Console.WriteLine("Hello {0}", /*вивід першого аргументу*/ args[0]);
			//Програма зупиниться і буде чекати вводу користувачем любих даних
			Console.ReadKey();
		}
	}
}