﻿/*
 Тема уроку
 Функції

 Види функцій
 Статичні і динамічні
 Статичні функції відносяться до класу
 Динамічні функції відносяться до об'єкту класу (екземпляру)
 З параметрами і без параметрів
 З поверненням та без повернення
 За типом доступу(private protected public internal)
 public - функція загальнодоступна

 Правила
 Правило 1 - функцію можна оголошувати тільки всередині певного класу
 Правило 2 - тип повернення завжди один
 Правило 3 - всі параметри функції повинні бути строготипізованими
 Правило 4 - типи у функції з параметрами можуть бути різними
 Правило 5 - параметрів може бути безліч і всі вони розділяються комами
 */
using System;

namespace Lesson5
{
	class Program
	{
		#region Functions

		#region NonParametles functions

		//[специфікатор доступу] [модифікатор доступу] [тип повернення] [назва ф-ції]()
		//{
		//}

		/// <summary>
		/// Статична функція без параметрів і без повернення
		/// </summary>
		public static void PrintHelloWorld()
		{
			Console.WriteLine("Hello world");
		}

		/// <summary>
		/// Статична функція без параметрів з поверненням стрічки
		/// </summary>
		/// <returns>Стрічка привітання з світом</returns>
		public static string GetHelloWorldString()
		{
			return "Hello World";
		}

		public static int GetHelloWorldLength()
		{
			return "Hello World".Length;
		}

		#endregion

		#region Parametles functions

		/// <summary>
		/// This function will add two values
		/// Метод без повернення з двома параметрами
		/// </summary>
		/// <param name="x">First value to be added</param>
		/// <param name="y">Second value to be added</param>
		public static void PrintSum(int x, int y)
		{
			var result = x + y;
			Console.WriteLine(result);
		}

		/// <summary>
		/// This method will print string value to console many times
		/// </summary>
		/// <param name="inputText">Text to be printed</param>
		/// <param name="count">Count to print text</param>
		public static void PrintStringXTimes(string inputText, int count)
		{
			for (int i = 0; i < count; i++)
			{
				Console.WriteLine(inputText);
			}
		}


		#endregion

		#endregion

		static void Main(string[] args)
		{
			Program.PrintHelloWorld();
			//PrintHelloWorld(); можна не писати Program бо ми і так в класі Program

			var stringHello = GetHelloWorldString();
			Console.WriteLine(stringHello);

			Console.WriteLine(GetHelloWorldLength());

			for (int i = 0; i < 10; i++)
			{
				PrintSum(50, i);
			}

			PrintStringXTimes("Vova i Igor krasavchiki", 10);
			Console.ReadKey();
		}
	}
}
