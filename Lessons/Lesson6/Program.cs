﻿//------------------------------
//Базові поняття про виключення та їх обробка
//------------------------------

using System;

//Правило1 - якщо пишеться try то має писатися або catch або finally
//Правило2 - можна комбінувати загальними та конкретними обробниками виключень
//Правило3 - якщо прописано загальний обробник, то конкретні після нього писати неможна
			//іншими словами всі загальні обробники мають іти після конкретних
namespace Lesson6
{
	class Program
	{
		static void Main(string[] args)
		{
			bool notFinished = true;

			//try catch
			var intArray = new[] { 10, 20, 30 };

			while (notFinished)
			{
				Console.WriteLine("Виберіть елемент масиву");
				try
				{
					//Код який може згенерувати виключення FormatException
					var index = Convert.ToInt32(Console.ReadLine());
					var test = 15 / index;
					//Код який може згенерувати виключення IndexOutOfRangeException
					var element = intArray[index];
					Console.WriteLine($"Вибраний елемент {index}={element}");
					notFinished = false;
				}
				
				//Обробник конкретного виключення 1
				catch (IndexOutOfRangeException myException1)
				{
					//додатковий код для запобігання проблеми, що виникла за рахунок генерації виключення
					Console.WriteLine(myException1.Message);
					//TODO Описати дані об'єкту виключення
				}
				//Обробник конкретного виключення 2
				catch (FormatException myException2)
				{
					Console.WriteLine("Ти лох. Введи число");
				}
				//Загальний обробник виключення
				catch (Exception ex)
				{
					Console.WriteLine("Ти лох {0}", ex.Message);
				}
			}



			//приклади
			try
			{
				//Код що генерує виключення або може бути аварійним
			}
			catch //по дефолту - це базовий обробник
			{
				//Код що обробляє виключення
			}
			finally
			{
				//Код що повинен виконатися в любому випадку, 
				//і при винекненні виключення і при його відсутності
			}

			//Можна писати без catch
			try
			{
			}
			finally
			{
			}

			//Обробка виключень може бути вкладеною
			///Block start
			try
			{
				//Block A
				try
				{
					//Block B
					try
					{
						//Block C
						//Exception
						//1 Якщо тут виникло виключення, то програма починає шукати обробник в межах блоку B
					}
					catch //2 Якщо цей обробник не підходить то він шукається на вищому рівні в блоці А, коли підходить , то пехід до 3
					{
						//3 виконається і перейде до блоку End якщо обробник підійшов в пункті 2
					}
				}
				catch //4 Якщо цей обробник не підходить то він шукається на вищому рівні в блоці start, коли підходить , то пехід до блоку 5
				{
					//5 виконається і перейде до блоку End якщо обробник підійшов в пункті 2
				}
			}
			catch
			{
			}
			//Block End

			Console.WriteLine("Програму виконано");
			Console.ReadKey();
		}
	}
}
