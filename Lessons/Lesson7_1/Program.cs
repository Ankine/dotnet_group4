﻿//------------------------------
//Базові поняття ООП
//------------------------------

/*
 Унаслідування - це можливість створювати нові класи та об'єкти використовуючи дані інших об'єктів
 Інкапсуляція - це можливість приховати частину реалізаці (коду/функцій/полів/методів) об'єкта в скередині об'єкта
 Поліморфізм - це можливість у кожного об'єкта мати свою поведінку навіть, якщо виклик цієї поведінки виглядає однаково
				- це можливість присвоювати змінним батьківського класу об'єкти нащадків
 */

/*
 * static virtual abstract sealed
 * 
 * static - Показує, що член класу/структури відноситься до самого класу а не до об'єктів цього класу
 * virtual - Віртуальний член класу, що може бути перевизначений в нащадку
 * abstract - Абстрактий член - не має реалізації і повинен бути реалізованим в нащадку
 *			  Абстрактним може бути лише член абстрактного класу
 *			  Абстрактний клас не може створювати екземплярів а лише використовується для унаслідування
 *			  Абстрактний член зобов'язує всіх нащадків себе реалізувати
 * sealed - Закрита сутність - не можна використати унаслідуваня
 */

using System;
using System.ComponentModel;

namespace Lesson7_1
{
	//Специфікатори доступу
	//public - загальнодоступна сутність (всі зборки)
	//private - закарита від всіх сутність (інкапсульована). окрім того об'єкта де вона оголошена
	//protected - сутність доступна для поточного об'єкту та для об'єктів нащадків (породжених або унаслідуваних)
	//internal - застосовується для сутностей (класів/структур/перечислень/делегатів/тощо) і означає, 
	//що сутність доступна тільки в межах поточної зборки
	//protected internal - застосовується для членів сутностей і означає, що член доступний в 
	//межах поточної зборки і для використання в нащадках інших збірок
	public abstract class Person //Базовий клас
	{
		public Person()
		{
			TotalPersonsCreated++;
		}

		public static int TotalPersonsCreated;

		public string Name;

		protected string LastName;

		protected string TestInternal;

		private int age;
		public void SetAge(int age)
		{
			//перевірки
			this.age = age;
		}

		public int GetNameCount()
		{
			return Name.Length;
		}

		public virtual void SayHello()
		{
			Console.WriteLine("Hello I am a person");
		}

		public abstract string GetWorkPosition();
	}

	internal class Student : Person
	{
		public string LearningPlaceName;

		public string GetLastName()
		{
			return LastName;
		}

		public override string GetWorkPosition()
		{
			return "Student";
		}

		public override void SayHello()
		{
			Console.WriteLine("Hello I am Student");
		}
	}

	public sealed class Worker : Person
	{
		public override void SayHello()
		{
			Console.WriteLine("Hello I am Worker");
		}

		public override string GetWorkPosition()
		{
			return "Worker";
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			var person1 = new Student();
			var person2 = new Student();
			var person3 = new Student();
			var person4 = new Student();

			Console.WriteLine(Person.TotalPersonsCreated);

			var student = new Student();
			//Ми можемо вказати Name бо воно загальнодоступне в Person
			student.Name = "Sasha";
			var count = student.GetNameCount(); //5

			//Ми можемо вказати LearningPlaceName бо воно загальнодоступне в Student
			student.LearningPlaceName = "NUWMNRU";

			//student.LastName - недоступна

			student.SetAge(12);

			Person[] persons = new Person[2];
			persons[0] = new Student();
			persons[1] = new Worker();


			foreach (var person in persons)
			{
				person.SayHello();
				Console.WriteLine("My work positios is:");
				Console.WriteLine(person.GetWorkPosition());
			}

			Console.ReadKey();
		}
	}
}
