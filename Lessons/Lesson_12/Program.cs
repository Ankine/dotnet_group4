﻿/*
 * Анонімні методи (І коротко про Lambda Expressions)
 * Анонімним методом називається метод, який немає назви і використовує ключове слово delegate (це по суті літерал для делегата)
 * Аноні методи використовуються тільки у випадку, коли нема необхідності описувати стандартний метод, 
 * Анонымний метод дозволяє описати змінну для делегата любої складності
 * що належить класу.
*/

using System;

namespace Lesson_12
{
	class Program
	{
		public static void ExecuteAction(Action<string> someAction)
		{
			if (someAction != null)
			{
				someAction("Test string");
			}
		}

		public static void ExecuteFunction(Func<string, int , bool> someAction)
		{
			if (someAction != null)
			{
				var test = someAction("Test string", 10);
			}
		}

		static void Main(string[] args)
		{
			Action<string> action1 = delegate(string name)
			{
				Console.WriteLine("Notification received for: {0}", name);
			};

			Func<string, int, bool> func1 = delegate(string val1, int val2)
			{
				return true;
			};

			ExecuteAction(name => { Console.WriteLine($"MyName is {name}"); });
			
			//Після виклику цього методу test всередині ExecuteFunction стане false, бо "Test string" 11 а не 10
			ExecuteFunction((name,count) => name.Length == count);

			ExecuteAction(action1);
			ExecuteFunction(func1);


			Console.ReadKey();
		}
	}
}
