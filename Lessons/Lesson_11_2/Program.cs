﻿/*
 * Деталі Делегатів
 * 
 * Правило 1 - Якщо делегат має тип повернення і містить посилання на два або більше методів, а також виклик делегату 
 * має місце у виразі, то використовується лише останній метод, що був доданий до делегату
*/

using System;

namespace Lesson_11_2
{
	/// <summary>
	/// Делегат з типом повернення і параметром
	/// </summary>
	/// <param name="value">Параметр делегату</param>
	/// <returns>Повернення делегату</returns>
	public delegate string MyDelegate(int value);

	public delegate int CalculationFunction(int value1, int value2);

	public class Calculator
	{
		public int Value1 { get; set; }
		public int Value2 { get; set; }
		public int Result { get; set; }

		public void Reset()
		{
			Value1 = 0;
			Value2 = 0;
			Result = 0;
		}

		public void Calculate(CalculationFunction operation)
		{
			Result = operation(Value1, Value2);
		}

		public override string ToString()
		{
			return $"Value1 = {Value1} Value2 = {Value2} Result = {Result} ";
		}
	}

	public static class CalcFunction
	{
		public static int Add(int v1, int v2)
		{
			return v1 + v2;
		}

		public static int Mul(int v1, int v2)
		{
			return v1 * v2;
		}
	}

	class Program
	{
		public static MyDelegate TestDelegate { get; set; }

		public static string IntToString(int v)
		{
			Console.WriteLine($"IntToString");
			return $"Value is {v}";
		}

		public static string IntMult2ToString(int v)
		{
			Console.WriteLine($"IntMult2ToString");
			return $"Value *2 is {v*2}";
		}

		static void Main(string[] args)
		{
			TestDelegate += IntToString;
			TestDelegate += IntMult2ToString;

			Console.WriteLine(TestDelegate(2));

			Console.WriteLine(TestDelegate(5));


			var calc = new Calculator();

			calc.Value1 = 15;
			calc.Value2 = 20;
			calc.Calculate(CalcFunction.Add);
			Console.WriteLine(calc);
			Console.ReadKey();
		}
	}
}
