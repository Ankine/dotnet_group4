﻿/*
 Теми уроку
 Масиви
 
 Масив - це набір даних одного типу
 [0] при оголошенні змінної певного типу під нього виділяється певна кількість ОЗУ
 [0, 1, 2, 3] при оголошенні масиву певного типу виділяється кількість ОЗУ для цього типу* на кількість елементів масиву
 Правило 1 обов'язково вказувати кількість елементів масиву (може бути не явно)

 Правила
 Правило 2 обов'язково вказувати тип даних (може бути не явно)
 Правило 3 Якщо масив типу А, то він не може містити елементи типу Б
 */

using System;

namespace Lesson4
{

	class Program
	{
		static void Main(string[] args)
		{
			#region Одновимірні масиви

			//Оголошення масивів
			int[] intArray = new int[5]; // 0 0 0 0 0
			int[] intArray2 = new int[] {10, 21, 3, 4, 5}; //10 21 3 4 5
			var intArray_1 = new int[5]; // 0 0 0 0 0
			var intArray2_1 = new int[] { 10, 21, 3, 4, 5 }; //10 21 3 4 5
			int[] intArray3 = {10, 101, 25};
			var intArray4 = new int[]{ 10, 101, 25 };
			var array = new [] { 10, 101, 25, 0, 10 };
			

			//запис нового значення елементу
			array[2] = 40;
			array[3]++; //0+1=1;

			//array[1] - читання виклик по індексу або використання індексатора
			Console.WriteLine(array[1]); //101
			Console.WriteLine(array[2]); //40

			//Звернення до неіснуючого елементу масиву
			//var notExistingItem = array[5]; //неможна звертатися до неіснуючого елементу масиву


			#endregion

			#region Багатовимірні масиви

			//Павило1 двовимірний масив повинен бути прямокутним (фіксованої розмірності)
			//двохвимірний масив
			bool[,] x2Array1 = new bool[2, 2];  /* false  false
												   false  false */
			int[,] x2Array2 = new int[,] {{2, 1},{3, 5}}; //масив розміром 2*2
														  /* 2 1
															3 5
														   */
			int[,] x2Array3 = { { 2, 1, 3}, { 3, 5, 2 } }; //масив розміром 2*3
														   /* 2 1 3
															  3 5 2
															*/
			Console.WriteLine("Rank = " + x2Array3.Rank);
			int[,,,,] fuckArray = new int[1, 2, 4, 5, 8];
			var testValue = fuckArray[0, 1, 3, 4, 7];

			//звернення
			var value = x2Array3[1, 2]; //2
			var value2 = x2Array3[1, 0]; //3

			Console.WriteLine(value);

			#endregion

			#region Рваний масив

			int[][] test = new int[3][]
			{
				new int[1]{5},
				new int[]{5,5,4,5,6},
				new int[3]{1,8,5}
			};

			int[][,] test2 = new int[2][,]
			{
				new int[3, 2] {{1, 2}, {3, 4}, {5, 6}},
				new int[3, 2] {{7, 8}, {9, 10}, {11, 12}}
			};

			var testValue4 = test2[1][1, 0];//9

			#endregion

			#region Операції над масивами

			int[] testArray = new int[5];

			Console.WriteLine("Заповнення масиву");
			for (int i = 0; i < testArray.Length; i++)
			{
				Console.WriteLine("Please enter value for testArray[{0}] ",i);
				testArray[i] = Convert.ToInt32(Console.ReadLine());
			}
			Console.WriteLine("Виведення масиву");
			for (int i = 0; i < testArray.Length; i++)
			{
				Console.WriteLine("Element {0} of array is {1}", i, testArray[i]);
				//Console.WriteLine($"Array[{i}]=={testArray[i]}");
			}

			//testArray.Rank - кількість вимірів масиву

			#endregion


			Console.ReadKey();
		}
	}
}
