﻿//------------------------------
//Базові поняття про анонімні типи та структури
//------------------------------

using System;
using Lesson7_1;

//Анонімні типи
//Правила 1 Оголошувати змінні в середині анонімного типу можна тільки під час ініціалізації
//Правило 2 Не можна присвоювати існуючій змінній анонімного типу нове значення

//Структури
//Правило 1 Структура може мати конструктор, якщо конструктора нема, то всі поля структури ініціалізуються автоматом
//Правило 2 При наявності конструктора всі поля обов'язково повинні бути проініціалізовані
//Правило 3 Структура не підтримує класичного унаслідування
//Правило 4 Об'єкти структур містяться в стеку а не в кучі

namespace Lesson7
{
	struct Person
	{
		public Person(string name, string lastName, int age, bool isChaynik = false)
		{
			Name = name;
			LastName = lastName;
			Age = age;
			IsChaynik = isChaynik;
		}

		public string Name;
		public string LastName;
		public int Age;
		public bool IsChaynik;
	}

	class Program
	{
		public static void WritePerson(Person person)
		{
			Console.WriteLine($"{person.Name} {person.LastName} {person.Age}");
		}

		static void Main(string[] args)
		{
			#region AnonimusTypes

			//Змінна анонімного типу
			var person1 = new //ключове стово для створення об'єкту
			{//тіло для створення об'єкту називається ініціалізатором
				Name = "Sasha",
				LastName = "Bovgyria",
				Age = 25
			};

			//Правило 2 Не можна присвоювати існуючій змінній анонімного типу нове значення
			//person1.Age = 26; неможна

			//Правило 1 Оголошувати змінні в середині анонімного типу можна тільки під час ініціалізації
			//person1.Height = "sdfgd";

			var person2 = new
			{
				Name = "Vova",
				LastName = "Bovgyria",
				Age = 29,
				Height = 195
			};

			//person2.Age = 25; 

			Console.WriteLine($"{person1.Name} {person1.LastName} {person1.Age}");

			#endregion

			#region Structures

			var s_person1 = new Person("Vova","Bovgyria",29);
			var s_person2 = new Person();
			
			WritePerson(s_person1);
			WritePerson(s_person2);

			#endregion

			Console.Read();


			//Тест internal
			//var student = new Student(); - не доступний
			var person = new Person();
			var lstName = person.Name;
	

		}
	}
}
