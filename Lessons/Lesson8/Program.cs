﻿using System;
using System.Collections.Generic;

// Клас (class) - це специфікація на об'єкт
// Екземпляр (Instance) - це об'єкт, який створений за класом (специфікацією)
// Ініціалізація - присвоєння об'єкту змінній
// Контсруюваня - це процес створення об'єкта
// 
// Всі класи неявно унаслідують клас Object
// 
namespace Lesson8
{
	#region Simple Nested

	/// <summary>
	/// Звичайний клас
	/// </summary>
	public class MyTestClass : Object
	{
		/// <summary>
		/// Вкладений клас
		/// </summary>
		protected class NestedClass
		{
		}
	}

	#endregion

	#region Static

	/// <summary>
	/// Статичний
	/// Не можна унаслідувати несатичні класи в тому числі і Object
	/// Неможливо створити екземпляр статичного класу
	/// Всі члени статичного класу повинні бути статичними
	/// </summary>
	public static class MyStaticClass
	{
		public static void TestStaticMethod()
		{
		}
	}

	#endregion

	#region Abstract

	/// <summary>
	/// Неможливо створити екземпляр абстрактого класу
	/// бо цей клас може використовуватися тільки в якості базового класу (унаслідування)
	/// Абстрактні члени можуть бути тільки всередині абстрактоного класу
	/// Абстрактний член не має реалізації
	/// Абстрактний клас зобов'язує всі класи нащадки реалізувати всі абстрактні члени
	/// </summary>
	public abstract class MyAbstractAnimal : Object
	{
		//Базовий функціонал
		public void MyNonAbstractMethod()
		{
		}

		public abstract void SayHello();
	}

	public class Cat : MyAbstractAnimal
	{
		public override void SayHello()
		{
			Console.WriteLine("Hello I am cat");
		}
	}

	public class Dog : MyAbstractAnimal
	{
		public override void SayHello()
		{
			Console.WriteLine("Hello I am dog");
		}
	}

	#endregion

	#region Sealed

	/// <summary>
	/// Запакований клас не дозволяє унаслідувати себе
	/// </summary>
	public sealed class MySealedClass : MyTestClass
	{
	}

	#endregion

	#region Generic

	public class MyGenericClass<TValueType>
	{
		public TValueType Value { get; set; }

		public void SetValue(TValueType newValue)
		{
			Value = newValue;
		}
	}

	#endregion

	class Program
	{
		static void Main(string[] args)
		{
			var val1 = new MyTestClass();
			Console.WriteLine(val1.ToString());

			//Виклик статичного методу
			MyStaticClass.TestStaticMethod();

			//Неможна створити екземпляр абстрактого класу
			//var val2 = new MyAbstractClass();

			var animals = new List<MyAbstractAnimal>
			{
				new Cat(),
				new Dog()
			};

			foreach (var myAbstractAnimal in animals)
			{
				myAbstractAnimal.SayHello();
			}

			var generic_string = new MyGenericClass<string>();
			generic_string.SetValue("sdfgsdfgsdf");

			var generic_int = new MyGenericClass<int>();
			generic_int.SetValue(10);

			Console.ReadKey();
		}
	}
}
