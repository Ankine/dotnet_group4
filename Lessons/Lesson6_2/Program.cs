﻿//------------------------------
//Тонкості обробки виключень
//------------------------------

using System;

namespace Lesson6_2
{
	class Program
	{
		public static void Method1()
		{
			Console.WriteLine("From Method 1");
			Method2();
		}

		public static void Method2()
		{
			try
			{
				//Код, що генерує виключення
				Console.WriteLine("From Method 2");
				throw new Exception("Some exception");
				Console.WriteLine("Текст що не виведеться");
			}
			catch (StackOverflowException ex)
			{
			}
			catch (Exception ex)
			{
				Console.WriteLine("Level1 {0}", ex.Message);
				throw;
				throw ex; //перенаправляємо виключення на рівень вище
				//throw new Exception("Another exception"); // генерація нового виключення
			}

		}

		static void Main(string[] args)
		{
			//Виключерння можна перенаправляти
			try
			{
				Method1();
			}
			catch (Exception ex)
			{
				//Console.WriteLine("Level2 {0}", ex.Message);
				Console.WriteLine(ex.StackTrace);
			}
			Console.ReadKey();
		}
	}
}
