﻿/* 
 Теми уроку
 Перетворення подібного типу шляхом приведення
 Використання методів Parse/TryParse відносно типів та їх різниця
 Стандартні значення типів
*/


using System;

namespace Lesson_3
{
	class Program
	{
		static void Main(string[] args)
		{
			#region Перетворення числових тіпів

			#region Явне приведення

			//У випадку, якщо змінна може містити значення іншої змінної без втрати даних,
			//то додаткових маніпуляцій з перетворенням робити не потрібно
			byte byteValue = 255;

			var integerValue = byteValue; //255

			double doubleValue = integerValue; //255

			//Потрібні додаткові маніпуляції за рахунок можливої втрати даних
			doubleValue = doubleValue + 0.5; //255.5

			//операція (тип для приведення)змінна для приведення -  називається cast або explicit convertion 
			byte value2 = (byte)doubleValue; //valeu2 = 255         0.5 втрачається

			int intValue = (int)doubleValue; //(int) якщо хочемо отримати цілу частину дробового числа

			//При оголошенні літерала float потрібно використовувати приведенна або символ F
			float floatValue = (float)5.5;
			float floatValue2 = 5.5F;

			#endregion

			#region Convert
			//Клас Convert призначений для  конвертації стрічок у заданий стандартний тип
			string inputValue = "15";
			string inputValue2 = "False";

			int val1 = Convert.ToInt32(inputValue);
			short val2 = Convert.ToInt16(inputValue);
			ushort val3 = Convert.ToUInt16(inputValue);
			bool val4 = Convert.ToBoolean(1); //true
			bool val5 = Convert.ToBoolean(0); //false
			bool val6 = Convert.ToBoolean(inputValue2);

			#endregion

			#region Parse/TryParse

			int expectedValue;
			//Метод Parse теж є не надійним за рахунок можливості винекнення виключення
			expectedValue = int.Parse(inputValue);

			string inputValue3 = Console.ReadLine();

			//TryParse зконвертує стрічку inputValue3 в int тільки в тому випадку, якщо це можливо без помилок
			int.TryParse(inputValue3, out expectedValue); //expectedValue буде 0 у випадку невдачі

			bool expectedBoolValue;
			if (bool.TryParse("True", out expectedBoolValue))
			{
				//використання expectedBoolValue
			}

			Console.ReadKey();

			#endregion

			#region Default values
			//Стандартні типи можуть бути не проініцілізовані, 
			//в такому випадку вони отримують значення по замовчуванні
			//bool - false;
			//int/short/long - 0;
			//double - 0.0
			//string - null
			//char - 0

			#endregion

			#endregion
		}
	}
}