﻿/**
 * Поля та властивості класів 
 * Коструктори
 * 
 */

using System;

namespace Lesson8_1
{
	public enum Sex : byte
	{
		Male,
		Female
	}


	public class Person
	{
		private const string TEST = "<XML>";
		private const int CountEyes = 2;


		//Конструктор без параметрів дозволяє використовувати ініціалізатор
		public Person()
		{
			Person.totalPersons++;
		}

		//функція створення об'єкта
		public Person(string name, int age)
		{
			this.name = name;
			this.age = age;
		}

		public static int totalPersons;

		public Sex Sex;

		//Поле класу
		public string name;

		/// <summary>
		/// Поле тільки для читання і присвоюватися може тільки в кострукторі
		/// подільші зміни значення поля заборонені
		/// </summary>
		public readonly int age;

		/// <summary>
		/// Поле тільки для читання але присвоєння відбувається прямо при оголошенні
		/// </summary>
		public const int max_age = 10;

		private string _heirColor;

		//Властивість це член класу, що дозволяє інкапсулювати певне поле класу і відфільтрувати неправильні дані
		//повна властивість
		public string HeirColor
		{
			set
			{
				if (value.Length <= 5)
				{
					_heirColor = value;
				}
			}
			get { return _heirColor; }
		}

		//Спрощена властивість AutoProperty
		public string SimpleProperty { get; set; }

		//GetProperty - властивість що дозволяє вичитувати значення
		public string GetProperty
		{
			get { return "test"; }
		}
		//SetProperty - властивість що дозволяє встановлювати значення
		public string SetProperty
		{
			set { _heirColor = value; }
		}

		//GetProperty - властивість що дозволяє вичитувати значення представлена лямда виразом
		//private string testField;
		public string LambdaProp => "test";

		//Повна властивість представлена лямда виразами
		public string Lambda2
		{
			set => _heirColor = value;
			get => _heirColor;
		}

		public override string ToString()
		{
			return $"Person: Name {name}; Age:{age}";
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			var person = new Person("Sasha", 27);

			person.HeirColor = "value";
			person.HeirColor = "value2";
			Console.WriteLine($"Person: Name {person.name}; HeirColor:{person.HeirColor}");
			Console.WriteLine(person);
			Console.ReadKey();
		}
	}
}
