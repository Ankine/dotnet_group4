﻿/*
 * Делегати
 * 
 * Правило 1 - В першу чергу делегати призначені для збереження посилання на метод або декілька методів
 * Правило 2 - Делегат можна використовувати будь де, де використовуються інші типи (параметер/властивість/тип повернення/поле)
 * Правило 3 - До змінної делегато можна додати лише посилання на той метод, який підходить по сигнатурі до делегату
 * Правило 4 - для додавання використовується += для того, щоб забрати метод використовується -=
*/

using System;

namespace Lesson_11
{
	public delegate void TestDelegate();

	public class Person
	{
		public TestDelegate DoWork { get; set; }
	}


	public static class Company
	{
		public static void CleanBuilding()
		{
			Console.WriteLine("CleanBuilding");
		}

		public static void PrintDocuments()
		{
			Console.WriteLine("PrintDocuments");
		}

		public static void WritePrograms()
		{
			Console.WriteLine("WritePrograms");
		}

		public static void LogWork()
		{
			Console.WriteLine("LogWork");
		}

		public static string CannotUse()
		{
			return "CannotUse";
		}
	}



	class Program
	{
		static void Main(string[] args)
		{
			var person1 = new Person();
			var person2 = new Person();

			person1.DoWork += Company.CleanBuilding;
			person1.DoWork += Company.LogWork;
			person2.DoWork += Company.WritePrograms;
			person2.DoWork += Company.PrintDocuments;
			person2.DoWork += Company.LogWork;

			person1.DoWork();
			Console.WriteLine();
			person2.DoWork();
			//person2.DoWork.Invoke();
			foreach (var invocationMember in person2.DoWork.GetInvocationList())
			{
				Console.WriteLine(invocationMember.Method);
			}



			Console.ReadKey();
		}
	}
}
