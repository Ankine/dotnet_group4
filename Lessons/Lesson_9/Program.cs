﻿/*
 * Правило 1 - Функція для створення нового об'єкту повинна мати таку ж само назву як і клас
 * ця функція називається конструктором
 * Правило 2 - Вважається що дефолтний конструктор без параметрів оголошений неявно
 * Правило 3 - Написання коснтруктора з параметрами відміняє правило 2
 * Правило 4 - можна використовувати конструктор а потім ініціалізатор, якщо це не суперечить базовим правилам
 */

using System;

namespace Lesson_9
{
	public abstract class Human : Object
	{
		private string _name;
		private int _age;

		/// <summary>
		/// 
		/// </summary>
		protected Human()
			:this("NoName", 0)
		{
			Console.WriteLine("Empty Human ctor");
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="age"></param>
		public Human(string name, int age):base()
		{
			Console.WriteLine("Human ctor with parameters");
			_name = name;
			_age = age;
		}

		public string LastName { get; set; }
	}

	public class Person : Human
	{
		
		static Person()
		{
			Console.WriteLine("Static Person ctor");
		}

		/// <summary>
		/// Creates simple person object
		/// </summary>
		public Person()
		{
			Console.WriteLine("Empty Person ctor");
		}

		/// <summary>
		/// Creates person object with name
		/// </summary>
		/// <param name="name">Name of person</param>
		public Person(string name)
		{
			Console.WriteLine("Empty Person ctor");
		}

		/// <summary>
		/// Creates person object with name and age specified
		/// </summary>
		/// <param name="name">Name of person</param>
		/// <param name="age">Age of person</param>
		public Person(string name, int age)
		{
			Console.WriteLine("Empty Person ctor");
		}
	}


	class Program
	{
		static void Main(string[] args)
		{
			var person1 = new Person();


			var person2 = new Person();

			var person3 = new Person("name", 25)
			{
				LastName = "test"
			};
	
			Console.ReadKey();
		}
	}
}
