﻿/*
 Теми уроку:
 Базові типи даних в C#
 Типи даних в .NET
 Етика назв змінних
 */

using System;

namespace Lesson1_1
{
	class Program
	{
		/// <summary>
		/// camelCase - testExtensionObject
		/// PascalCase - TestExtensionObject
		/// </summary>
		static void Main(string[] args)
		{
			//Локальны змінні прийнято називати в camel Case

			#region Логічні змінні

			//Синтаксис оголошення змінної - тип назва = значення
			bool booleanValue1; //оголошення змінної
			booleanValue1 = false; //ініціалізація
			
			//Змінна логічного типу , що приймає true або false займає 1 bit
			bool booleanValue = true;

			#endregion

			#region Числові змінні

			#region Цілочисельні

			//Змінна типу байту, що приймає число від 0-255 займає 1 byte або 8 bits
			byte byteValue = 255;
			Byte typeValueNet = 255;

			//Змінна типу sbyte, що приймає число від -128-127 займає 1 byte або 8 bits
			sbyte sbyteValue = -128;
			SByte sbyteValueNet = -128;

			//Змінна типу short, що приймає число від -2^16/2 до 2^16/2-1 займає 16 bits
			short shortValue1 = 32767;
			short shortValue2 = -32768;
			Int16 shortValueNet = 3522;

			//Змінна типу ushort, що приймає число від 0 до 2^16-1 займає 16 bits
			ushort ushortValue = 65000;
			UInt16 ushortValueNet = 65000;

			//Змінна типу int, що приймає число від -2^32/2 до 2^32/2-1 займає 32 bits
			int intValue = 15654;
			Int32 intValueNet = 15654;

			//Змінна типу uint, що приймає число від 0 до 2^32 займає 32 bits
			uint uintValue = 15654;
			UInt32 uintValueNet = 15654;

			//Змінна типу long, що приймає число від -2^64/2 до 2^64/2-1 займає 64 bits
			long longValue = 15654;
			Int64 longValueNet = 15654;

			//Змінна типу ulong, що приймає число від 0 до 2^64 займає 64 bits
			ulong ulongValue = 15654;
			UInt64 ulongValueNet = 15654;

			#endregion

			#region З плаваючою комою

			//Дробове число 32 bytes
			float floatValue1 = 2.456545654565F;
			float floatValue2 = (float)2.5;
			//Дробове число 64 bytes
			double doubleValue = 2.5;
			//Не запам'ятовувати
			decimal decimalValue = new decimal(2.5);

			#endregion

			#endregion

			#region Строкові змінні

			string stringValue = "test string";
			String netString = "testStirng";

			char charValue = 'c';

			//Код символу
			byte charCode = (byte)charValue;

			#endregion

			#region Автоматичне визначення змінної  - анонімний тип

			//var myValue0; //Помилка, бо компілятор неможе визначити тип

			var myValue1 = "hello vova"; //int myValue1 = true
			var myValue2 = 1; //int myValue2 = true
			var myValue3 = true; //bool myValue3 = true

			//myValue1 = 10; Помилка, бо myValue1 строковий тип // error
			
			//переприсвоєння значення
			myValue2 = 50;
			myValue1 = "hello sasha";
			myValue3 = false;

			#endregion

			Console.ReadKey();
		}
	}
}