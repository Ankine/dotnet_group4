﻿using System;
using System.IO;


/*
 byte.TryParse("string", out dayOfBirth) - true - якщо вдалося перетворити стрічку в дані для вказаного типу
										   false - якщо перетворення не вдалося
	while(true)
	{
	}
	 */
namespace Examples
{
	class Program
	{
		static void Main(string[] args)
		{
			int documentSerial;
			string firstName;
			string lastName;
			string fatherName;

			DateTime dayOfBirth;
			string city;
			byte houseNumber;
			string street;
			byte flatNumber;
			string carNumer;

			Console.WriteLine("          Постанова про адмiнiстративне правопорушення          ");

			Console.WriteLine("Введіть серію постанови");
			while (!int.TryParse(Console.ReadLine(), out documentSerial))
			{
				Console.WriteLine("Помилка, введiть число");
			}

			Console.WriteLine("Введіть ім'я особи");
			firstName = Console.ReadLine();
			Console.WriteLine("Введіть прізвище особи");
			lastName = Console.ReadLine();
			Console.WriteLine("Введіть по батькові особи");
			fatherName = Console.ReadLine();

			Console.WriteLine("Введіть дату народження особи");
			while (!DateTime.TryParse(Console.ReadLine(), out dayOfBirth))
			{
				Console.WriteLine("Помилка, введiть коректну дату");
			}

			Console.WriteLine("Введіть місто проживання особи");
			city = Console.ReadLine();

			Console.WriteLine("Введіть місто вулицю особи");
			street = Console.ReadLine();

			Console.WriteLine("Введіть номер будинку");
			while (!byte.TryParse(Console.ReadLine(), out houseNumber))
			{
				Console.WriteLine("Помилка, введiть число");
			}

			Console.WriteLine("Введіть номер квартири");
			while (!byte.TryParse(Console.ReadLine(), out flatNumber))
			{
				Console.WriteLine("Помилка, введiть число");
			}
			Console.WriteLine("Введіть номер реєстрації автомобіля");
			carNumer = Console.ReadLine();

			//Робота з файлами, буде вивчатися пізніше
			using (var file = new StreamWriter($"{documentSerial}.txt", true))
			{
				file.WriteLine($"Серія постанови {documentSerial} дата {DateTime.Now}");
				file.WriteLine($"Прізвище особи {lastName}");
				file.WriteLine($"Ім'я особи {firstName}");
				file.WriteLine($"По батькові особи {fatherName}");
			}

			Console.WriteLine("Документ збережено");
			Console.ReadKey();
		}
	}
}
