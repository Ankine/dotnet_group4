﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Номер документу");
			string docummentNumber = Console.ReadLine();
			Console.WriteLine("Диагноз");
			string dg = Console.ReadLine();

			Console.WriteLine("Призвище имя та по батькови пациєнта: ");
			string fullName = Console.ReadLine();

			Console.WriteLine("Стать: ");
			string sex = Console.ReadLine();

			Console.WriteLine("Вiк: ");
			int age;
			while(!int.TryParse(Console.ReadLine(), out age) || age<0 || age>120)
			{
				Console.WriteLine("Вiк введено не правильно. Повторіть спробу");
			}

			Console.WriteLine("Адреса, населений пункт");
			string address = Console.ReadLine();

			Console.WriteLine("Назва и адреса мисця роботи: ");
			string workPlaceAddress = Console.ReadLine();

			Console.WriteLine("Дата захворювання");
			DateTime date;
			while (!DateTime.TryParse(Console.ReadLine(), out date))
			{
				Console.WriteLine("Дату введено не правильно. Повторіть спробу");
			}

			Console.WriteLine("Мисце госпитализации");
			string hospitalPlace = Console.ReadLine();
			
			string result = string.Empty;

			while (!(result == "1" || result == "2"))
			{
				Console.WriteLine("Чи було отруєння");
				Console.WriteLine("1 Так");
				Console.WriteLine("2 Ні");
				result = Console.ReadLine();
			}

			bool isPoisoned = result == "1" ? true : false;
			string poisoneing = string.Empty;
			if (isPoisoned)
			{
				Console.WriteLine("Чим отруєний потерпілий");
				poisoneing = Console.ReadLine();
			}

			using (var file = new StreamWriter($"{docummentNumber}.txt", true))
			{
				file.WriteLine($"Диагноз {dg}");
				file.WriteLine($"Призвище имя та по батькови пациєнта {fullName}");
				file.WriteLine($"Стать пацієнта {sex}");
				file.WriteLine($"Вік {age}");
				file.WriteLine($"Адреса {address}");
			}

			Console.ReadKey();
		}
	}
}
