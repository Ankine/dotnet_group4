﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintedEdition
{
	public abstract class PrintedEdition
	{
        protected string NamePrintedEdition;
        protected int NumberOfPages;

        

    }

	public class Book: PrintedEdition
	{
        public Book(string name, int numberOfPages) : base()
        {
            Console.WriteLine("Fantasy book");
            NamePrintedEdition = name;
            NumberOfPages = numberOfPages;
        }

        public Book(string name, int numberOfPages, string listOfAuthors) : base()
        {
            Console.WriteLine("Scientific book");
            NamePrintedEdition = name;
            NumberOfPages = numberOfPages;
            this.ListOfAuthors = listOfAuthors;
        }

        public string ListOfAuthors { get; }
    }

	public class Journal: PrintedEdition
	{

	}

	public class Newspaper : PrintedEdition
	{

	}

















    class Program
	{
		static void Main(string[] args)
		{
		}
	}
}
