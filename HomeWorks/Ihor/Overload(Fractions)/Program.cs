﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Overload_Fractions_
{
    public class Fraction
    {
        public Fraction(int x, int y)
        {
            X = x;
            Y = y;
            if (y == 0)
            {
                y = 1;
                Console.WriteLine("Недопустиме значення для знаменника:{0}. Замiнюємо на 1", y);
            }
            else if(y < 0)
            {
                x = -x;
                y = -y;
            }
        }

        public int X { get; set; }
        public int Y { get; set; }
       

        public static Fraction operator +(Fraction a, Fraction b)
        {
            int t1 = a.X * b.Y + a.Y * b.X;
            int t2 = a.Y * b.Y;
            return new Fraction(t1, t2);
        }
        /*public static Fraction operator +(int a, Fraction b)
        {
            return a + new Fraction(b);
        }*/
        public static Fraction operator -(Fraction a, Fraction b)
        {
            int t1 = a.X * b.Y - a.Y * b.X;
            int t2 = a.Y * b.Y;
            return new Fraction(t1, t2);
        }
        public static Fraction operator *(Fraction a, Fraction b)
        {
            int t1 = a.X * b.X;
            int t2 = a.Y * b.Y;
            return new Fraction(t1, t2);
        }
        public static Fraction operator /(Fraction a, Fraction b)
        {
            int t1 = a.X * b.Y;
            int t2 = a.Y * b.X;
            return new Fraction(t1, t2);
        }
        
        public static Fraction operator -(Fraction a)
        {
            return new Fraction(-a.X, a.Y);
        }


        public static bool operator >(Fraction a, Fraction b)
        {
            if (a.X * b.Y > a.Y * b.X)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator <(Fraction a, Fraction b)
        {
            if (a.X * b.Y < a.Y * b.X)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator ==(Fraction a, Fraction b)
        {
            if (a.X == b.X && a.Y == b.X)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator !=(Fraction a, Fraction b)
        {
            if (a.X != b.X || a.Y != b.X)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator >=(Fraction a, Fraction b)
        {
            if (a.X * b.Y >= a.Y * b.X)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator <=(Fraction a, Fraction b)
        {
            if (a.X * b.Y <= a.Y * b.X)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return X.ToString() + "/" + Y.ToString();
        }
    }

        class Program
        {
            static void Main(string[] args)
            {
            Fraction a = new Fraction(1, 2);
            Fraction b = new Fraction(1, 4);
            Fraction c = a + b;
            Fraction c1 = a - b;
            Fraction c2 = a * b;
            Fraction c3 = a / b;
            Fraction c4 = -a;
            Console.WriteLine($"Додавання дробiв c= {c}");
            Console.WriteLine($"Вiднiмання дробiв c1= {c1}");
            Console.WriteLine($"Множення дробiв c2= {c2}");
            Console.WriteLine($"Дiлення дробiв c3= {c3}");
            Console.WriteLine($"c4= {c4}");
            Console.WriteLine(a > b);
            Console.WriteLine(a < b);
            Console.WriteLine(a == b);
            Console.WriteLine(a != b);
            Console.WriteLine(a >= b);
            Console.WriteLine(a <= b);


            Console.ReadKey();
            }
        }
    }

