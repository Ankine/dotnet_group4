﻿using System;

namespace HomeWork02_ClassCalculator
{
	public class ProgrammerCalculator : Calculator
	{
		#region CalcFunctionWithOneOperand

		public static string Bin(string val1)
		{
			return Convert.ToString(Convert.ToInt64(val1), 2);
		}

		public static string Oct(string val1)
		{
			return Convert.ToString(Convert.ToInt64(val1), 8);
		}

		public static string Hex(string val1)
		{
			return Convert.ToString(Convert.ToInt64(val1), 16);
		}

		#endregion
	}
}