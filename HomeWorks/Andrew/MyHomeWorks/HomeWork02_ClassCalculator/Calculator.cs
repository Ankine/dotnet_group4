﻿namespace HomeWork02_ClassCalculator
{
	public abstract class Calculator
	{
		#region Delegates
		public delegate string NoneOperandCalculations();

		public delegate string OneOperandCalculations(string value1);

		public delegate string TwoOperandsCalculations(string value1, string value2);

		#endregion

		#region Properties

		public string Value1 { get; set; }

		public string Value2 { get; set; }

		public string Result { get; private set; }

		#endregion

		#region Constructors

		public Calculator()
		{
			Value1 = null;
			Value2 = null;
		}

		#endregion

		#region Methods

		public void Calculate(NoneOperandCalculations operation)
		{
			if (Value1 == null)
			{
				Value1 = operation();
			}
			else
			{
				Value2 = operation();
			}
		}

		public void Calculate(OneOperandCalculations operation)
		{

			Result = operation(Value1);
		}

		public void Calculate(TwoOperandsCalculations operation)
		{
			Result = operation(Value1, Value2);
		}

		public virtual void Reset()
		{
			Value1 = null;
			Value2 = null;
		}

		#endregion

	}
}