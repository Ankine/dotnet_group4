﻿using System;

namespace HomeWork02_ClassCalculator
{
	public class StandartCalculator : Calculator
	{

		#region CalcFunctionWithOneOperand

		public static string Sqrt(double value1)
		{
			return Convert.ToString(Math.Sqrt(value1));
		}

		#endregion

		#region CalcFunctionWithTwoOperands

		public static string Add(string val1, string val2)
		{
			double a;
			double b;
			var x = double.TryParse(val1, out a);
			var y = double.TryParse(val2, out b);
			var result = a + b;
			return Convert.ToString(result);
		}

		public static string Sub(string val1, string val2)
		{
			return Convert.ToString(Convert.ToDouble(val1) - Convert.ToDouble(val2));
		}

		public static string Mult(string val1, string val2)
		{
			return Convert.ToString(Convert.ToDouble(val1) * Convert.ToDouble(val2));
		}

		public static string Div(string val1, string val2)
		{
			if (Convert.ToDouble(val2) == 0)
			{
				throw new DivideByZeroException("На нуль дiлити неможна!!!");
			}

			return Convert.ToString(Convert.ToDouble(val1) / Convert.ToDouble(val2));

		}

		#endregion


	}
}