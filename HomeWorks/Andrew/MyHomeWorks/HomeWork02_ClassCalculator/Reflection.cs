﻿using System;
using System.Reflection;

namespace HomeWork02_ClassCalculator
{
	public class Reflection
	{
		//Рефлексії не місце в продакшен коді (це діч)
		// Данный метод виводить iнформацiю про методи, що мiстяться в класi
		public static void MethodReflectInfo<T>(T obj)
			where T : class
		{
			Type t = typeof(T);

			// Отримуємо колекцiю методiв
			MethodInfo[] MArr = t.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

			Console.WriteLine("\n*** Доступнi функцii калькулятора ***\n");

			// Виводимо методи

			foreach (MethodInfo m in MArr)

			{
				Console.WriteLine(" {0}", m.Name);
			}

			Console.WriteLine(string.Empty);
		}
	}
}