﻿using System;

namespace HomeWork02_ClassCalculator
{
	public class ScientificCalculator : StandartCalculator
	{
		#region CalcFunctionWithoutOperands

		public static string Pi()
		{
			return Convert.ToString(Math.PI);
		}

		#endregion

		#region CalcFunctionWithOneOperand
		/// <summary>
		/// Returns the sine of the specified angle.
		/// </summary>
		/// <param name="value1">An angle, measured in degrees.</param>
		/// <returns></returns>
		public static string Sin(double? value1) => Convert.ToString(Math.Sin((double)value1 * Math.PI / 180));

		/// <summary>
		/// Returns the cosine of the specified angle.
		/// </summary>
		/// <param name="value1">An angle, measured in degrees.</param>
		/// <returns></returns>
		public static string Cos(double? value1) => Convert.ToString(Math.Cos((double)value1 * Math.PI / 180));

		/// <summary>
		/// Returns the tangent of the specified angle.
		/// </summary>
		/// <param name="value1">An angle, measured in degrees.</param>
		/// <returns></returns>
		public static string Tan(double? value1) => Convert.ToString(Math.Tan((double)value1 * Math.PI / 180));


		#endregion

	}
}