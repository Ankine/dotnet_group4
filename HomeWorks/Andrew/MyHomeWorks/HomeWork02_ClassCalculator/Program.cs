﻿using System;

/// <summary>
/// Оаерації треба зробити окремим класами при чому різні операції можуть підтримуватися різними калькуляторами
/// н/п юазова операція для всії операцій StandartCalculator буде StandarеOperation
/// тоді в класі калькулятора StandartCalculator буде колекцыя з StandarеOperation (List<StandarеOperation>)
/// </summary>
namespace HomeWork02_ClassCalculator
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				do
				{
					#region Menu

					Console.WriteLine("Оберiть тип калькулятора\n");
					Console.WriteLine("1 - Стандартний");
					Console.WriteLine("2 - Iнженерний");
					Console.WriteLine("3 - Програмiстський\n");
					Calculator calc = null;

					//неможна використовувати оператори розгалуження там де проблема можна вирішити поліморфізмом
					short calcTypeNumber = 0;
					var isValid = false;
					while (!isValid)
					{
						if (short.TryParse(Console.ReadLine(), out short value))
						{
							switch (calcTypeNumber)
							{
								case 1: calc = new StandartCalculator(); break;
								case 2: calc = new ScientificCalculator(); break;
								case 3: calc = new ProgrammerCalculator(); break;
							}
							isValid = true;
						}
						else
						{
							Console.WriteLine("Не обрано тип калькулятора!!!\n");
						}
						calcTypeNumber = value;
						if (isValid)
						{
							Reflection.MethodReflectInfo(calc);
						}
					}

					#endregion

					Console.WriteLine("Введiть значення першого операнду");
					calc.Value1 = Console.ReadLine();

					Console.WriteLine("Введiть значення другого операнду");
					calc.Value2 = Console.ReadLine();

					Console.WriteLine("Оберiть операцiю");
					var operation = Console.ReadLine();

					//всі види операцій можна представляти перечисленнями для того щоб не було magic strings
					if (operation == "Add")
					{
						calc.Calculate(StandartCalculator.Add);
						//Цю інформацію може повертати сам клас калькулятора
						Console.WriteLine("Результат: {0} + {1} = {2}", calc.Value1, calc.Value2, calc.Result);
						calc.Reset();
					}
					Console.WriteLine("\nДля выходу натиснiть Escape; для продовження - будь яку iншу клавiшу\n");
				}
				while (Console.ReadKey().Key != ConsoleKey.Escape);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.ReadLine();
			}
		}
	}
}
