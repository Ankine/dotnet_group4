﻿using System;
using System.Collections.Generic;
using HomeWork01;

namespace HomeWork01
{
	#region Enumerations

	public enum PowerButton : byte
	{
		TurnOff,
		TurnOn
	}

	public enum CentralLocking
	{
		Lock,
		Unlock
	}

	public enum LeftRightButtons : byte
	{
		Left,
		Right
	}

	public enum UpDownButtons : byte
	{
		Up,
		Down
	}

	public enum NumericKeypad
	{
		Zero,
		One,
		Two,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine
	}

	public enum RemoteDeviceTypes
	{
		TVSet,
		MusicCentre,
		CarCentralLocking
	}

	#endregion

	#region Interfaces

	public interface IVolumeControlable
	{
		string SetVolume(LeftRightButtons click);

	}

	public interface IProgramsSwitchable
	{
		string SetProgram(UpDownButtons click);

		string SetProgram(NumericKeypad click);
	}

	#endregion

	public abstract class RemoteControl
	{
		protected RemoteControl(RemoteDeviceTypes type)
		{
			DeviceType = type;
		}

		#region Properties

		//Назва властивості не выдповыдаэ суті
		protected RemoteDeviceTypes DeviceType { get; set; }

		protected PowerButton PowerStatus { get; set; }

		//Назва властивості не відповідає суті SetTVChanel() або CurrentTVChanel {get;set;}
		//Базовий пыльт знає про істування пульта до телевізора
		protected byte SetCurrentTVChanel { get; set; }

		protected byte SetCurrentRadioChanel { get; set; }

		protected byte VolumeLevel { get; set; }

		protected CentralLocking LockTheCar { get; set; }

		#endregion
	}

	public class SoundRemoteControl : RemoteControl, IVolumeControlable
	{
		public SoundRemoteControl(RemoteDeviceTypes type)
			: base(type)
		{
		}

		public string SetVolume(LeftRightButtons click)
		{
			if (click == LeftRightButtons.Right)
			{
				return $"Current TV volume is {++VolumeLevel}";
			}

			else
			{
				return $"Current TV volume is {--VolumeLevel}";
			}
		}
	}

	public class TVRemoteControl : SoundRemoteControl, IProgramsSwitchable
	{
		#region Constructor

		public TVRemoteControl()
			: base(RemoteDeviceTypes.TVSet)
		{
			SetCurrentTVChanel = 1;
			VolumeLevel = 10;
		}

		#endregion

		#region Methods

		public string SetProgram(UpDownButtons click)
		{
			if (click == UpDownButtons.Up)
			{
				return $"You are watching {++SetCurrentTVChanel} TV channels";
			}
			else
			{
				return $"You are watching {--SetCurrentTVChanel} TV channels";
			}
		}

		public string SetProgram(NumericKeypad click)
		{
			return $"You are watching {SetCurrentTVChanel = (byte)click} TV channels";
		}

		#endregion

		#region Overrides

		public override string ToString()
		{
			return $"The {DeviceType} is {PowerStatus} || Current TVChanel {SetCurrentTVChanel} || Current VolumeLevel {VolumeLevel}";
		}

		#endregion
	}

	public class MusicCentreRemoteControl : SoundRemoteControl, IProgramsSwitchable
	{
		#region Constructor

		public MusicCentreRemoteControl()
			: base(RemoteDeviceTypes.MusicCentre)
		{
			SetCurrentRadioChanel = 1;
			VolumeLevel = 10;
		}

		#endregion

		#region Mhetods

		public string GetStatusString(int chanelnumber)
		{
			return $"You are listening  {chanelnumber} Radio channel";
		}

		public string SetProgram(UpDownButtons click)
		{
			if (click == UpDownButtons.Up)
			{
				return GetStatusString(++SetCurrentRadioChanel);
			}
			else
			{
				return GetStatusString(--SetCurrentRadioChanel);
			}
		}

		public string SetProgram(NumericKeypad click)
		{
			SetCurrentRadioChanel = (byte)click;
			return GetStatusString(SetCurrentRadioChanel);
		}

		#endregion

		#region Overrides

		public override string ToString()
		{
			return $"The {DeviceType} is {PowerStatus} || Current RadioChanel {SetCurrentRadioChanel} || Current VolumeLevel {VolumeLevel}";
		}

		#endregion
	}

	public class CarCentralLockingRemoteControl : RemoteControl
	{
		#region Constructor

		public CarCentralLockingRemoteControl()
			: base(RemoteDeviceTypes.CarCentralLocking)
		{
			PowerStatus = PowerButton.TurnOff;
			LockTheCar = CentralLocking.Lock;
		}

		#endregion

		#region Methods

		public string LockOrUnlockTheCar(CentralLocking value)
		{
			LockTheCar = value;

			if (LockTheCar == CentralLocking.Unlock)
			{
				return $"The car is open";
			}
			else
			{
				return $"The car is closed";
			}
		}

		#endregion

		#region Overrides

		public override string ToString()
		{
			return $"The {DeviceType} is {PowerStatus}|| The Car Now is {LockTheCar}";
		}

		#endregion
	}
}

class Program
{
	static void Main(string[] args)
	{
		var a = new TVRemoteControl();

		var b = new MusicCentreRemoteControl();

		var c = new CarCentralLockingRemoteControl();

		Console.WriteLine(a.SetProgram(NumericKeypad.Five));

		Console.WriteLine(a.SetProgram(UpDownButtons.Up));

		Console.WriteLine(a.SetVolume(LeftRightButtons.Left));

		Console.WriteLine(a.SetVolume(LeftRightButtons.Left));

		Console.WriteLine(a.SetVolume(LeftRightButtons.Right));

		Console.WriteLine(b.SetVolume(LeftRightButtons.Left));

		Console.WriteLine(b.SetVolume(LeftRightButtons.Left));

		Console.WriteLine(b.SetVolume(LeftRightButtons.Left));

		Console.WriteLine(b.SetProgram(UpDownButtons.Up));

		Console.WriteLine(b.SetProgram(UpDownButtons.Up));

		Console.WriteLine(b.SetProgram(UpDownButtons.Up));

		Console.WriteLine(b.SetProgram(UpDownButtons.Down));

		Console.WriteLine(c.LockOrUnlockTheCar(CentralLocking.Unlock));

		Console.WriteLine(c.LockOrUnlockTheCar(CentralLocking.Lock));

		Console.WriteLine(string.Empty);

		var array = new List<RemoteControl>();
		array.Add(a);
		array.Add(b);
		array.Add(c);

		foreach (var i in array)
		{
			Console.WriteLine(i);
		}

		Console.WriteLine(string.Empty);

		Console.WriteLine("Remote control with the ability to adjust the volume");

		Console.WriteLine(string.Empty);

		var array2 = new List<IProgramsSwitchable>();

		array2.Add(a);

		array2.Add(b);

		foreach (var i in array2)
		{
			Console.WriteLine(i.SetProgram(UpDownButtons.Down));
		}

		Console.ReadKey();
	}
}