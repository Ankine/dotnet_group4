﻿using System;

namespace HomeWork03_OperatorsOverloading
{
	/*
	 * Написати клас прямокутника і перегрузити усі можливі оператори для нього. При чому логіка перегрузки самих операторів не важлива,
	 * головне перегрузитити їх усі та продимонструвати приклад цієї перегрузки в клієнтському коді.
	 */

	public class Rectangle
	{
		#region Data

		public double SideA { get; set; }

		public double SideB { get; set; }

		private double _area;

		private double _perimeter;

		#endregion

		#region Constructor

		public Rectangle(double sideA, double sideB)
		{
			SideA = sideA;
			SideB = sideB;
			GetArea();
			GetPerimeter();
		}

		#endregion

		#region Methods

		public double GetArea()
		{
			_area = SideA * SideB;
			return _area;
		}

		public double GetPerimeter()
		{
			_perimeter = (SideA + SideB) * 2;
			return _perimeter;
		}

		#endregion

		#region Operators overloading

		public static Rectangle operator +(Rectangle x, Rectangle y)
		{
			var a = x.SideA;
			var b = y._area / x.SideA + x.SideB;
			return new Rectangle(a, b);
		}

		public static Rectangle operator -(Rectangle x, Rectangle y)
		{
			if (x._area > y._area)
			{
				var a = x.SideA;
				var b = x.SideB - y._area / x.SideA;
				return new Rectangle(a, b);
			}
			else
			{
				var a = y.SideA;
				var b = y.SideB - x._area / y.SideA;
				return new Rectangle(a, b);
			}
		}

		public static Rectangle operator ++(Rectangle x)
		{
			return new Rectangle(x.SideA++, x.SideB++);
		}

		public static Rectangle operator --(Rectangle x)
		{
			if (x.SideA > 1 && x.SideB > 1)
			{
				return new Rectangle(x.SideA--, x.SideB--);
			}

			return new Rectangle(1, 1);
		}

		public static bool operator true(Rectangle x)
		{
			if (x.SideA > 0 && x.SideB > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator false(Rectangle x)
		{
			return !true;
		}

		public static Rectangle operator *(Rectangle x, Rectangle y)
		{
			return new Rectangle(x.SideA * y.SideA, x.SideB * y.SideB);
		}

		public static Rectangle operator /(Rectangle x, Rectangle y)
		{
			return new Rectangle(x.SideA / y.SideA, x.SideB / y.SideB);
		}

		public static bool operator ==(Rectangle x, Rectangle y)
		{
			if (x._area == y._area)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator !=(Rectangle x, Rectangle y)
		{
			return !true;
		}

		public static bool operator >(Rectangle x, Rectangle y)
		{
			if (x._area > y._area)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator <(Rectangle x, Rectangle y)
		{
			if (x._area < y._area)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator >=(Rectangle x, Rectangle y)
		{
			if (x._area >= y._area)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator <=(Rectangle x, Rectangle y)
		{
			if (x._area <= y._area)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public double this[int index]
		{
			get
			{
				if (index == 0)
					return SideA;
				if (index == 1)
					return SideB;

				Console.WriteLine("Неправильний iндекс {0}", index);
				return -1;
			}

			set
			{
				if (index == 0)
					SideA = value;
				if (index == 1)
					SideB = value;
				if (index != 0 && index != 1)
					Console.WriteLine("Неправильний iндекс {0}", index);
			}
		}

		#endregion

		#region Overrides

		public override string ToString()
		{
			return $"A {SideA}, B {SideB}, S {_area}, P {_perimeter}";
		}

		#endregion

	}


	class Program
	{
		static void Main(string[] args)
		{
			var rec1 = new Rectangle(2, 4);
			var rec2 = new Rectangle(1, 3);
			var rec4 = new Rectangle(1, 3);
			var rec5 = new Rectangle(4, 1);

			Console.WriteLine($"rec1 ({rec1}) + rec2 ({rec2}) = rec3 ({rec1 + rec2})\n");

			Console.ReadKey();
		}
	}
}
