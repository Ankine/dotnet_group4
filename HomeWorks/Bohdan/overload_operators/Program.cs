﻿/*
 *Object.GetHashCode() ---- ?
 */
using System;

namespace overload_operators
{
    public struct Point
    {
        public double x;
        public double y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }


    public class Vector
    {
        public Point coord;
     
        public Vector(double startX , double startY, double endX , double endY ):this(endX - startX, endY - startY)
        { }

        public Vector(Point start, Point end):this(start.x-end.x,start.y-end.y)
        { }

        public Vector(double _x,double _y)
        {
            coord.x = _x;
            coord.y = _y;
        }

        public double GetLenght()
        {
            return Math.Sqrt(Math.Pow(coord.x, 2) + Math.Pow(coord.y, 2));
        }

        public double AngleInRadians()
        {
            /*Vector A = new Vector(coord.x, 0);
            return Math.Acos(this * A / (GetLenght() * A.GetLenght()));*/
            if (0 <= coord.x && 0 <= coord.y)
            {
                Console.WriteLine("1   "+Math.Acos(coord.x / GetLenght()));
                return Math.Acos(coord.x / GetLenght());
            }
            else
            {
                if (0 > coord.x && 0 <= coord.y)
                {
                    Console.WriteLine("2   "+Math.Acos(coord.x / GetLenght()));
                    return Math.Acos(coord.x / GetLenght()) ;
                }
                else
                {
                    if (0 >= coord.x && 0 > coord.y)
                    {
                        Console.WriteLine("3   "+Math.Acos(coord.x / GetLenght()));
                        return Math.Acos(coord.x / GetLenght())+Math.PI/2 ;
                    }
                    else
                    {
                        Console.WriteLine("4   "+Math.Acos(coord.x / GetLenght()));
                        return Math.Acos(coord.x / GetLenght()) + 3*Math.PI/2 ;
                    }
                }
            }
        }

        public double AngleInGraduses()
        {
            return AngleInRadians() * 180 / Math.PI;
        }
        
        #region operators
        public static double operator *(Vector v1,Vector v2)
        {
            return v1.coord.x * v2.coord.x + v1.coord.y + v2.coord.y;
        }

        public static Vector operator +(Vector v1,Vector v2)
        {
            return new Vector(v1.coord.x + v2.coord.x, v2.coord.y + v2.coord.y);
        }

        public static Vector operator -(Vector v1)
        {
            return new Vector(-v1.coord.x, -v1.coord.y);
        }

        public static Vector operator *(double number,Vector v)
        {
            return new Vector(v.coord.x * number, v.coord.y * number);
        }

        public static Vector operator *(Vector v,double number)
        {
            return new Vector(v.coord.x * number, v.coord.y * number);
        }

        public static Vector operator /(Vector v1, double number)
        {
            return new Vector(v1.coord.x / number, v1.coord.y / number);
        }

        public static Vector operator -(Vector v1, Vector v2)
        {
            return new Vector(v1.coord.x - v2.coord.x, v2.coord.y - v2.coord.y);
        }

        public static bool operator ==(Vector v1, Vector v2)
        {
            if(v1.GetLenght() == v2.GetLenght() && v1.AngleInRadians() == v2.AngleInRadians())
            {
                return true;
            }
            return false;
        }

        public static bool operator !=(Vector v1, Vector v2)
        {
            if (v1.GetLenght() != v2.GetLenght() || v1.AngleInRadians() != v2.AngleInRadians())
            {
                return true;
            }
            return false;
        }

        public static bool operator >=(Vector v1,Vector v2)
        {
            if(v1.AngleInRadians()==v2.AngleInRadians()&&v1.GetLenght()>=v2.GetLenght())
            {
                return true;
            }
            return false;
        }

        public static bool operator <=(Vector v1, Vector v2)
        {
            if (v1.AngleInRadians() == v2.AngleInRadians() && v1.GetLenght() <= v2.GetLenght())
            {
                return true;
            }
            return false;
        }

        public static bool operator >(Vector v1, Vector v2)
        {
            if (v1.AngleInRadians() == v2.AngleInRadians() && v1.GetLenght() > v2.GetLenght())
            {
                return true;
            }
            return false;
        }

        public static bool operator <(Vector v1, Vector v2)
        {
            if (v1.AngleInRadians() == v2.AngleInRadians() && v1.GetLenght() < v2.GetLenght())
            {
                return true;
            }
            return false;
        }
        #endregion

        public override string ToString()
        {
            return $"Coordinates of vector ({coord.x},{coord.y}), angle= {AngleInRadians()}  lenght= {GetLenght()}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Vector A = new Vector(1,-1);
            Vector B = new Vector(1,1,2, 2);
            Vector C = A - B;
            Console.WriteLine("A  /n"+A+"B  /n"+B);
            Console.WriteLine("5 * B/n"+(5 * B));
            Console.WriteLine("A+B/n"+(A + B));
            Console.WriteLine("A*B/n"+(A * B));
            Console.WriteLine("A==B/n"+(A == B));
            Console.WriteLine("A!=B/n"+(A != B));
            Console.WriteLine("A >= B/n"+(A >= B));
            Console.WriteLine("A <= B/n"+(A <= B));
            Console.WriteLine("A < B/n"+(A < B));
            Console.WriteLine("A > B/n"+(A > B));

            Console.ReadKey();
        }
    }
}
