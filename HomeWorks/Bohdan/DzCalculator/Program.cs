﻿using System;

namespace DzCalculator
{
	class Program
	{
		public static bool GetValue(string text, out int value)
		{
			Console.Write($"{text} :\t");
			try
			{
				value = Convert.ToInt32(Console.ReadLine());
				return true;
			}
			catch (FormatException e)
			{
				Console.WriteLine(e.Message);
			}
			value = 0;
			return false;
		}

		static void Main(string[] args)
		{
			#region version 1

			Calculator calc = null;
			int choose = 0;
			bool trueInput = false;

			//Неможна використовувати одну і ту саму змінну для різних задам
			while (!trueInput)
			{
				Console.Clear();
				Console.WriteLine("\t Choose calc version: \n");

				// Це потрыбно винести у перечислення
				Console.WriteLine("Choose a CalculateFunction:\n\n" +
				                  "Addition \t\t 1 \n" +
				                  "Subtraction \t\t 2 \n" +
				                  "Division \t\t 3 \n" +
				                  "Mulmultiplication \t 4 \n\n" +

				                  "Sinus  \t\t\t 5 \n" +
				                  "Cosinus \t\t 6 \n" +
				                  "Tangens \t\t 7 \n" +
				                  "Cotangens \t\t 8 \n");
				try
				{
					choose = Convert.ToInt32(Console.ReadLine());
				}
				catch (FormatException e)
				{
					Console.Clear();
					Console.WriteLine(e.Message);
					Console.ReadKey();
				}
				//Неправильний порядок згідно бізнес логіки
				if (choose > 0 && choose <= 8)
				{
					trueInput = !trueInput;
				}
			}

			if (choose < 5)
			{
				var standart = new StandartCalculator();
				while (GetValue("Input Value 1", out int value))
				{
					//Випарвино - Повторне використання коду
					standart.Value1 = value;
				}

				while (GetValue("Input Value 2", out int value))
				{
					standart.Value2 = value;
				}
				calc = standart;
			}

			if (choose >= 5)
			{
				calc = new ScientificCalculator();
			}

			//Це повинно бути реалізовано за допомогою поліморфізму
			//Проблема в тому, що для додавання нової функції треба міняти кучу коду
			switch (choose)
			{
				case 1:
					calc.Calculate(StandartCalcFunction.Addition);
					break;
				case 2:
					calc.Calculate(StandartCalcFunction.Subtraction);
					break;
				case 3:
					calc.Calculate(StandartCalcFunction.Division);
					break;
				case 4:
					calc.Calculate(StandartCalcFunction.Mulmultiplication);
					break;
				case 5:
					calc.Calculate(ScientificCalcFunction.Sin);
					break;
				case 6:
					calc.Calculate(ScientificCalcFunction.Cos);
					break;
				case 7:
					calc.Calculate(ScientificCalcFunction.Tang);
					break;
				case 8:
					calc.Calculate(ScientificCalcFunction.Cotang);
					break;
			}
			Console.WriteLine(calc);
			Console.ReadKey();

			#endregion

		}
	}
}
