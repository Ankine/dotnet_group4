﻿using System;

namespace DzCalculator
{
	public abstract class Calculator
	{
		public double Value1 { get; set; }
		public double Rezult { get; set; }

		protected abstract double Calculate();

		public virtual void Reset()
		{
			Value1 = 0;
			Rezult = 0;
			Console.Clear();
		}
	}

	public class StandartCalculator : Calculator
	{
		public double Value2 { get; set; }
		public StandartCalculateFunction Function { get; set; }

		public override void Reset()
		{
			base.Reset();
			Value2 = 0;
		}

		protected override double Calculate()
		{
			if (Function != null)
			{
				return Function(Value1,Value2);
			}
			return 0;
		}

		public override string ToString()
		{
			return $"Value1 = {Value1} Value2 = {Value2} Result = {Rezult} ";
		}
	}

	public class ScientificCalculator : Calculator
	{
		public Func<double, double> Function { get; set; }

		protected override double Calculate()
		{
			Function?.Invoke(Value1);
			return 0;
		}

		public override string ToString()
		{
			return $"Value1 = {Value1} Result = {Rezult} ";
		}
	}
}