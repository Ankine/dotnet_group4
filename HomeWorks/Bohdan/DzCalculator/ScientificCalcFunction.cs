﻿using System;

//Це скоріш всього має бути не статичним для того щоб реалізувати поліморфізм
public class ScientificCalcFunction
{
	public static double Sin(double v1)
	{
		return Math.Sin(v1);
	}

	public static double Cos(double v1)
	{
		return Math.Cos(v1);
	}

	public static double Tang(double v1)
	{
		return Math.Tan(v1);
	}

	public static double Cotang(double v1)
	{
		return 1 / Math.Tan(v1);
	}
}