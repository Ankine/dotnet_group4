﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bohdan
{
	public abstract class Car
	{
		public int year;

		protected Car(string model, int year)
		{
			Model = model;
			SetYear(year);
		}

		public string Model { get; set; }

		public int GetYear()
		{
			return year;
		}

		public void SetYear(int value)
		{
			if (value > 1920 && value < DateTime.Now.Year + 1)
			{
				year = value;
			}
			else
			{
				Console.WriteLine("Wrong year");
			}
		}

		public virtual void Show()
		{
			Console.WriteLine("I am {0}  car", Model);
		}

	}

	public class ElectroCar : Car
	{
		private double capacityOfTheAccumulator;

		public ElectroCar(string model, int year) 
			: base(model, year)
		{
		}

		public override void Show()
		{
			Console.WriteLine("I am {0}  ElectroCar and I have {1} capacity of the accumulator", Model, capacityOfTheAccumulator);
		}
	}

	public class CargoItem
	{
		public double Mass { get; set; }
	}

	public class TruckCar : Car
	{
		private double maxMassOfCargo;

		public TruckCar(string model, int year) 
			: this(model, year, 300)
		{
			Cargos = new List<CargoItem>();
		}

		public TruckCar(string model, int year, double maxMassOfCargo)
			:base(model,year)
		{
			this.maxMassOfCargo = maxMassOfCargo;
		}

		public double GetMassOfCargo()
		{
			return Cargos.Sum(i => i.Mass);
		}

		public void AddCargo(CargoItem newCargo)
		{
			if (GetMassOfCargo() + newCargo.Mass < maxMassOfCargo)
			{
				Cargos.Add(newCargo);
			}
		}

		private List<CargoItem> Cargos { get; set; }

		public double MaxMassOfCargo => maxMassOfCargo;

		public override void Show()
		{
			Console.WriteLine("I am {0}  TruckCar: my mass of cargo is {1}", Model, MaxMassOfCargo);
		}
	}

	public class PassengerCar : Car
	{
		private int passenger;
		private int maxPassenger;

		public PassengerCar(string model, int year, int passenger, int maxPassenger) 
			: base(model, year)
		{
			this.passenger = passenger;
			this.maxPassenger = maxPassenger;
		}

		public int MaxPassenger
		{
			get => maxPassenger;
			set
			{
				if (value >= 0)
				{
					maxPassenger = value;
				}
				else
				{
					Console.WriteLine(" \nWrond value \n maximum passenger mast be > 0");
				}
			}
		}
		public int Passenger
		{
			get => passenger;
			set
			{
				if (value <= maxPassenger)
				{
					passenger = value;
				}
				else
				{
					passenger = 0;
				}
			}
		}

		public override void Show()
		{
			Console.WriteLine("I am {0}  MaxPassenger: my number of passengers is {1}", Model, passenger);

		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			var autopark = new Car[4];
			autopark[0] = new TruckCar("MODEL1", 1980);
			autopark[1] = new TruckCar("MODEL2", 1980, 2500);
			autopark[2] = new PassengerCar("MODEL3", 1980,5,15);
			autopark[3] = new ElectroCar("MODEL4", 2018);

			// truckCar.
			foreach (var i in autopark)
			{
				i.Show();
			}
			Console.ReadKey();
		}
	}
}
